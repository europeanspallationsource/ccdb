/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ui.common;

import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.UUID;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.junit.Before;
import org.junit.Test;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.GenericArtifact;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;


import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ui.common.AbstractAttributesController;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author georgweiss
 * Created Sep 12, 2018
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesContext.class})
public class AbstractAttributesControllerTest {
    
    @Before
    public void init(){
        PowerMockito.mockStatic(FacesContext.class);
        FacesContext facesContext = mock(FacesContext.class);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        Application application = mock(Application.class);
        when(facesContext.getApplication()).thenReturn(application);
    }
    

    @Test
    public void testParentHasNoExternalLinks(){
        
        ComponentType componentType =
                new ComponentType();
        componentType.setExternalLinkList(Collections.emptySet());
           
        assertTrue(AbstractAttributesController.isExternalLinkNameUnique(componentType.getExternalLinkList(), new ExternalLink()));
    }
    
    
    @Test
    public void testParentHasOneExternalLinkWithNonConflictingName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ExternalLink externalLink1 = new ExternalLink();
        externalLink1.setName("link1");
        externalLink1.setId("id1");
        componentType.setExternalLinkList(Sets.newHashSet(externalLink1));
        
        ExternalLink externalLink2 = new ExternalLink();
        externalLink2.setName("link2");
        
        assertTrue(AbstractAttributesController.isExternalLinkNameUnique(componentType.getExternalLinkList(), externalLink2));
    }
    
    
    @Test
    public void testParentHasOneExternalLinkWithConflictingName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ExternalLink externalLink1 = new ExternalLink();
        externalLink1.setName("link1");
        externalLink1.setId("id1");
        componentType.setExternalLinkList(Sets.newHashSet(externalLink1));
        
        ExternalLink externalLink2 = new ExternalLink();
        externalLink2.setName("link1");
             
        assertFalse(AbstractAttributesController.isExternalLinkNameUnique(componentType.getExternalLinkList(), externalLink2));
    }
    
    
    @Test
    public void testModifyExternalLinkWithSameName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ExternalLink externalLink1 = new ExternalLink();
        externalLink1.setName("link1");
        externalLink1.setId("id1");
        
        componentType.setExternalLinkList(Sets.newHashSet(externalLink1));
        
        
        ExternalLink externalLink2 = new ExternalLink();
        externalLink2.setName("link1");
        externalLink2.setId("id1");
        
        assertTrue(AbstractAttributesController.isExternalLinkNameUnique(componentType.getExternalLinkList(), externalLink2));
    }
    
    
    
    @Test
    public void testModifyExternalLinkWithNewName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ExternalLink externalLink1 = new ExternalLink();
        externalLink1.setName("link1");
        externalLink1.setId("id1");
        componentType.setExternalLinkList(Sets.newHashSet(externalLink1));
        
        
        ExternalLink externalLink2 = new ExternalLink();
        externalLink2.setName("link2");
        externalLink2.setId("id1");
        
        assertTrue(AbstractAttributesController.isExternalLinkNameUnique(componentType.getExternalLinkList(), externalLink2));
    }
    
    
    @Test
    public void testModifyExternalLinkWithConflictingName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ExternalLink externalLink1 = new ExternalLink();
        externalLink1.setName("link1");
        externalLink1.setId("id1");
        ExternalLink externalLink2 = new ExternalLink();
        externalLink2.setName("link2");
        externalLink2.setId("id2");
        componentType.setExternalLinkList(Sets.newHashSet(externalLink1, externalLink2));
        
        ExternalLink externalLink3 = new ExternalLink();
        externalLink3.setName("link1");
        externalLink3.setId("id2");
        
        
        assertFalse(AbstractAttributesController.isExternalLinkNameUnique(componentType.getExternalLinkList(), externalLink3));
    }

}
