/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.views;

import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.UUID;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.junit.Before;
import org.junit.Test;
import org.openepics.discs.conf.ent.ComponentType;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;


import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author georgweiss
 * Created Sep 12, 2018
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesContext.class})
public class EntityAttrArtifactViewTest {
    
    @Before
    public void init(){
        PowerMockito.mockStatic(FacesContext.class);
        FacesContext facesContext = mock(FacesContext.class);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        Application application = mock(Application.class);
        when(facesContext.getApplication()).thenReturn(application);
    }
    

    @Test
    public void testParentHasNoAttachements(){
        
        ComponentType componentType =
                new ComponentType();
        
        EntityAttrArtifactView view =
                new EntityAttrArtifactView(new ComptypeArtifact(), componentType, null);
        
        UploadedFile uploadedFile = mock(UploadedFile.class);
        when(uploadedFile.getFileName()).thenReturn("file1");
        
        FileUploadEvent event = new FileUploadEvent(mock(UIComponent.class), uploadedFile);
        
        assertFalse(view.uploadedFileRejected(event));
    }
    
    @Test
    public void testParentHasOneAttachmentWithNonConflictingName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ComptypeArtifact artifact1 = new ComptypeArtifact();
        artifact1.setName("file1");
        componentType.setEntityArtifactList(Arrays.asList(artifact1));
    
        EntityAttrArtifactView view =
                new EntityAttrArtifactView(new ComptypeArtifact(), componentType, null);
        
        UploadedFile uploadedFile = mock(UploadedFile.class);
        when(uploadedFile.getFileName()).thenReturn("file2");
        
        FileUploadEvent event = new FileUploadEvent(mock(UIComponent.class), uploadedFile);
        
        assertFalse(view.uploadedFileRejected(event));
    }
    
    @Test
    public void testParentHasOneAttachmentWithConflictingName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ComptypeArtifact artifact1 = new ComptypeArtifact();
        artifact1.setName("file1");
        componentType.setEntityArtifactList(Arrays.asList(artifact1));
    
        EntityAttrArtifactView view =
                new EntityAttrArtifactView(new ComptypeArtifact(), componentType, null);
        
        UploadedFile uploadedFile = mock(UploadedFile.class);
        when(uploadedFile.getFileName()).thenReturn("file1");
        
        FileUploadEvent event = new FileUploadEvent(mock(UIComponent.class), uploadedFile);
        
        assertTrue(view.uploadedFileRejected(event));
    }
    
    @Test
    public void testModifyArtifactWithSameFileName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ComptypeArtifact artifact1 = mock(ComptypeArtifact.class);
        when(artifact1.getName()).thenReturn("file1");
        when(artifact1.getId()).thenReturn(777L);
     
        componentType.setEntityArtifactList(Arrays.asList(artifact1));
    
        EntityAttrArtifactView view =
                new EntityAttrArtifactView(artifact1, componentType, null);
        
        UploadedFile uploadedFile = mock(UploadedFile.class);
        when(uploadedFile.getFileName()).thenReturn("file1");
        
        FileUploadEvent event = new FileUploadEvent(mock(UIComponent.class), uploadedFile);
        
        assertFalse(view.uploadedFileRejected(event));
    }
    
    @Test
    public void testModifyArtifactWithNewFileName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ComptypeArtifact artifact1 = new ComptypeArtifact();
        artifact1.setName("file1");
        componentType.setEntityArtifactList(Arrays.asList(artifact1));
    
        EntityAttrArtifactView view =
                new EntityAttrArtifactView(artifact1, componentType, null);
        
        UploadedFile uploadedFile = mock(UploadedFile.class);
        when(uploadedFile.getFileName()).thenReturn("file2");
        
        FileUploadEvent event = new FileUploadEvent(mock(UIComponent.class), uploadedFile);
        
        assertFalse(view.uploadedFileRejected(event));
    }
    
    @Test
    public void testModifyArtifactWithConflictingFileName(){
        
        ComponentType componentType =
                new ComponentType();
        
        ComptypeArtifact artifact1 = new ComptypeArtifact();
        artifact1.setName("file1");
        ComptypeArtifact artifact2 = new ComptypeArtifact();
        artifact2.setName("file2");
        componentType.setEntityArtifactList(Arrays.asList(artifact1, artifact2));
    
        EntityAttrArtifactView view =
                new EntityAttrArtifactView(artifact1, componentType, null);
        
        UploadedFile uploadedFile = mock(UploadedFile.class);
        when(uploadedFile.getFileName()).thenReturn("file2");
        
        FileUploadEvent event = new FileUploadEvent(mock(UIComponent.class), uploadedFile);
        
        assertTrue(view.uploadedFileRejected(event));
    }
}
