/**
 * 
 */

/** Global CCDB environment */
var CCDB = {
        "config" : {
            "jumpToElementOnLoad" : false,
            "tableHasContent": -1,
            "selectionInProgress" : false
        },
        "jumpToElementOnLoadHandler" : function(xhr, target) {
            selectEntityInTable(CCDB.dataLoaderInternal.dataKey, CCDB.dataLoaderInternal.tableVarName);
        },
        "jumpToElementOnLoadHandler2" : function(xhr, target) {
            selectEntityInTable2(CCDB.dataLoaderInternal.dataKey, CCDB.dataLoaderInternal.tableVarName);
        },
        "dataLoaderInternal" : {},
        "oldABHandler" : null,
        "disabledAjaxButton" : null,
        "newABHandler" : function(a, c) {
            if (a.s) {
                var sourceId = PrimeFaces.escapeClientId(a.s);
                var widget = $(sourceId);
                if (widget.hasClass("ui-button") && (CCDB.disabledAjaxButton == null)) {
                    CCDB.disabledAjaxButton = widget;
                    widget.removeClass('ui-state-hover ui-state-focus ui-state-active').addClass('ui-state-disabled')
                        .attr('disabled', 'disabled');
                    jQuery(document).on("pfAjaxComplete", CCDB.ajaxButtonHandler);
                }
            }
            CCDB.oldABHandler.call(PrimeFaces, a, c);
        },
        "takeOverAb" : function() {
            if (!CCDB.oldABHandler) {
                CCDB.oldABHandler = PrimeFaces.ab;
                PrimeFaces.ab = CCDB.newABHandler;
            }
        },
        "ajaxButtonHandler" : function(xhr, target, errorThrown) {
            if(!CCDB.disabledAjaxButton.hasClass("manualEnableButton")){
                CCDB.disabledAjaxButton.removeClass('ui-state-disabled').removeAttr('disabled');
            }
            CCDB.disabledAjaxButton = null;
            jQuery(document).off("pfAjaxComplete", CCDB.ajaxButtonHandler);
        },
        "delayedSpinnerTimeout" : null,
        "SI" : {
            "sessionInfoDuration" : (365 * 24 * 60 * 60 *1000),
            "currentSessionId" : null,
            "lastTableSelection" : [],
            "pageId" : "",
            "confirmedId" : null,
            "pageNameToPageId" : {
                "home.xhtml" : {
                    "id" : "hierarchies",
                    "tableWidgetVarName" : "hierarchyWidget",
                    "tabWidgetVarName" : "hirearchyTabsVar"
                },
                "devices.xhtml" : {
                    "id" : "devices", 
                    "tableWidgetVarName" : "devicesTableVar"
                },
                "device-types.xhtml" : {
                    "id" : "deviceTypes",
                    "tableWidgetVarName" : "deviceTypeTableVar"
                },
                "properties.xhtml" : {
                    "id" : "properties",
                    "tableWidgetVarName" : "propertiesTableVar"
                },
                "enums.xhtml" : {
                    "id" : "enums",
                    "tableWidgetVarName" : "enumsTableVar"
                },
                "units.xhtml" : {
                    "id" : "units",
                    "tableWidgetVarName" : "unitsTableVar"
                },
                "log.xhtml" : {
                    "id" : "log",
                    "tableWidgetVarName" : "logTableVar"
                }
            },
            "sessionInfo" : emptySessionInfo
        }
}

var emptySessionInfo = {
    "oldSessionId" : null,
    "hierarchies" : {
        "tabSelection" : 0,
        "selections" : [],
        "expanded" : [],
        "filter" : ""
    },
    "devices" : {
        "selections" : [],
        "filter" : {}
    },
    "deviceTypes" : {
        "selections" : [],
        "filter" : {}
    },
    "properties" : {
        "selections" : [],
        "filter" : {}
    },
    "enums" : {
        "selections" : [],
        "filter" : {}
    },
    "units" : {
        "selections" : [],
        "filter" : {}
    },
    "log" : {
        "filter" : {}
    }
}

function enableButton() {
    var button = $(".manualEnableButton");
    button.removeClass('ui-state-disabled').removeAttr('disabled');
}

function removeSessionIdAndParametersFromUrl() {
    var modification = false;
    var href = window.location.href;

    if (window.location.search != "") {
        href = href.replace(window.location.search, "");
        modification = true;
    }

    if (href.indexOf(";jsessionid") >= 0) {
        href = href.substring(0, href.indexOf( ";jsessionid" ));
        modification = true;
    }

    if (modification) {
        window.history.pushState("", "", href);
    }
}

function emHeight() {
    return $("#top").outerHeight(true) - $("#top").outerHeight();
}

function adjustFooterPosition() {
    var footerWidth = $(".footer-message").outerWidth(true);
    $(".footer-message").css({"left":(window.innerWidth-footerWidth)/2});
}

function startDownload() {
    PF("statusDialog").show();
}

function scrollSelectedIntoView(tableWidget, rowKey) {
    if (CCDB.config.selectionInProgress) setTimeout(function() {scrollSelectedIntoView(tableWidget);}, 100);

    var tableSelection = getTableSelection(tableWidget);
    if (!rowKey) rowKey = tableSelection[0];
    if (isRowSelected(tableWidget)) {
        var scrollableBodyHeight = tableWidget.scrollBody[0].clientHeight;
        var selectedNodeSelector = "tr[data-rk='" + rowKey + "']";
        var selectedNode = tableWidget.jq.find(selectedNodeSelector);
        if (selectedNode.length <= 0) return;
        var selectedNodePosition = selectedNode[0].offsetTop;
        var lowerLimit = Math.max(0, selectedNodePosition - scrollableBodyHeight + selectedNode.outerHeight());
        if (tableWidget.scrollBody.scrollTop() < lowerLimit || tableWidget.scrollBody.scrollTop() > selectedNodePosition) {
            tableWidget.scrollBody.scrollTop(Math.max(0, selectedNodePosition - scrollableBodyHeight / 2));
        }
    } else {
        tableWidget.scrollBody.scrollTop(0);
    }
}

/**
 * selectEntityInTable() only works on page load, so it can be used for navigating to an entity form another screen. 
 * @param dataKey the global index of the entity
 * @param tableVarName the name of the PrimeFaces table variable
 * @param force if defined and true, the method can be used even if the page has not been loaded
 */
function selectEntityInTable(dataKey, tableVarName, force) {
    if ((typeof force == 'boolean') && (force == true)) CCDB.config.jumpToElementOnLoad = true;
    // selectEntityInTable() only works on page load, so it can be used for navigating to
    //     an entity form another screen.
    bootstrapDataLoader(dataKey, tableVarName);
    var tableWidget = PF(tableVarName);
    // check whether the data is here. If not, wait a little...
    if ((CCDB.config.tableHasContent < 3) && (tableWidget.jq.find('tr.ui-widget-content').length <= 0)) {
        CCDB.config.tableHasContent++;
        setTimeout(function() {selectEntityInTable(dataKey, tableVarName, false);}, 300);
        return;
    }

    // table data loaded
    CCDB.config.tableHasContent = -1;
    // search for entity
    var rowRef = tableWidget.jq.find("tr[data-rk='" + dataKey + "']");

    if (rowRef.length > 0) {
        // entity found
        CCDB.config.selectionInProgress = false;
        jQuery(document).off("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler);
        tableWidget.unselectAllRows();
        tableWidget.selectRow(rowRef);
        scrollSelectedIntoView(tableWidget);
    } else if (!tableWidget.allLoadedLiveScroll) {
        // not found, but there's more data
        tableWidget.loadLiveRows();
    } else {
        // entity not found
        CCDB.config.selectionInProgress = false;
        jQuery(document).off("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler);
    }
}

function bootstrapDataLoader(dataKey, tableVarName) {
    if (CCDB.config.jumpToElementOnLoad == true) {
        CCDB.config.selectionInProgress = true;
        CCDB.config.jumpToElementOnLoad = false;
        // store the parameters
        CCDB.dataLoaderInternal = {
                "dataKey" : dataKey,
                "tableVarName" : tableVarName
        }
        jQuery(document).on("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler);
    }
}

function resizeDeleteList(deleteDialogId) {
    var deleteTable = $("#" + deleteDialogId + " .dialogListTable .ui-datatable-scrollable-body");
    var tableContentHeight = $("#" + deleteDialogId + " .dialogListTable .ui-datatable-data").outerHeight(true);
    var calculatedHeight = emHeight() * 13;
    if (tableContentHeight >= emHeight() * 25) {
        calculatedHeight = emHeight() * 25;
    } else if (tableContentHeight >= emHeight() * 13) {
        calculatedHeight = tableContentHeight + 2;
    }
    deleteTable.css({"height":calculatedHeight});
}

function initializeDataTable(scrollDataCount) {
    var datatable = PF('attributesDataTable');
    datatable.cfg.scrollLimit = scrollDataCount;
}

function dataTableLiveScrollFix() {
    var datatable = PF('attributesDataTable');
    // this is for PrimeFaces bug
    var actuallyLoaded = $(datatable.jqId).find(".ui-datatable-scrollable-body").
                                        find(".ui-datatable-data").find(".ui-widget-content").length;
    datatable.scrollOffset = actuallyLoaded;
    datatable.allLoadedLiveScroll = actuallyLoaded >= datatable.cfg.scrollLimit;
    datatable.shouldLiveScroll = !datatable.allLoadedLiveScroll;
}

function dataTableAjaxFix(xhr, status, args) {
    if (status && status.responseXML) {
        if (status.responseXML.getElementById('deviceTypesForm:attributesDataTable:attributesDataTable') || 
                status.responseXML.getElementById('devicesForm:attributesDataTable:attributesDataTable') || 
                status.responseXML.getElementById('hierarchies:content:attributesDataTable:attributesDataTable')) {
            dataTableLiveScrollFix();
        }
    }
}

function delayedSpinnerStart() {
    if (CCDB.delayedSpinnerTimeout == null) {
        CCDB.delayedSpinnerTimeout = setTimeout(function(){PF('statusDialog').show();} , 1100);
    }
}

function delayedSpinnerStop() {
    clearTimeout(CCDB.delayedSpinnerTimeout);
    CCDB.delayedSpinnerTimeout = null;
    PF('statusDialog').hide();
}

/**
 * Resets the table state for live scrolling. Setting all values the same as on the initial page load.
 * @param tableWidget
 */
function resetLiveTableState(tableWidget) {
    tableWidget.scrollOffset = 0;
    tableWidget.allLoadedLiveScroll = (tableWidget.cfg.scrollLimit <= tableWidget.scrollOffset - tableWidget.cfg.scrollStep);
    tableWidget.shouldLiveScroll = true;
    setTimeout(function() {scrollSelectedIntoView(tableWidget);}, 300);
}

function getTableSelection(tableWidget) {
    if (tableWidget != undefined){
        if (tableWidget.selection != undefined) {
            return tableWidget.selection;
        } else if (tableWidget.selections != undefined) {
            return tableWidget.selections;
        }
    }
    return [];
}

function isRowSelected(tableWidget) {
    return getTableSelection(tableWidget).length > 0;
}

function selectWithDelay(dataKey, tableVarName, delay) {
    setTimeout(function() {selectEntityInTable(dataKey, tableVarName, true);}, delay);
}

function getCookie(name) {
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookies = decodedCookie.split(';');
    for(var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        // trim leading spaces
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1);
        }
        if (cookie.indexOf(name + '=') == 0) {
            return cookie.substring(name.length + 1);
        }
    }
    return "";
}

function storeSessionInfo() {
    if (CCDB.SI.sessionInfo == null){
        clearCookie();
    }
    CCDB.SI.sessionInfo.oldSessionId = CCDB.SI.currentSessionId;
    var date = new Date();
    date.setTime(date.getTime() + CCDB.SI.sessionInfoDuration);
    document.cookie = "ccdbSessionInfo=" + encodeURIComponent(JSON.stringify(CCDB.SI.sessionInfo)) 
                        + ";expires=" + date.toUTCString() 
                        + ";path=/";
}

function loadSessionInfo() {
    CCDB.SI.currentSessionId = getCookie("JSESSIONID");
    var sessionCookie = getCookie("ccdbSessionInfo");
    // session info has not been stored yet
    if (sessionCookie == "") {
        storeSessionInfo();
        return;
    }
    CCDB.SI.pageId = getPageId();
    CCDB.SI.tableAction = "None"; // Possible actions "Select" and "Deselect"
    CCDB.SI.sessionInfo = JSON.parse(sessionCookie);
    CCDB.SI.lastTableSelection = getTableSelection(CCDB.SI.sessionInfo[CCDB.SI.pageId]).slice();  // what was the last selection on this page
}

function verifyId(pageId) {
    for (pageName in CCDB.SI.pageNameToPageId) {
        var id = CCDB.SI.pageNameToPageId[pageName].id;
        if (id == pageId) return true;
    }
    return false;
}

//returns the param if defined, or its default value if not. Returns 'null' on error.
function stringParamDefined(param, defaultValue, functionName) {
    if (!param) {
        if (!defaultValue || defaultValue.constructor !== String) {
            console.error("" + functionName + "()::stringParamDefined(): defaultValue not a string.");
            return null;
        }
        return defaultValue;
    } 
    if (param.constructor !== String) {
        console.error("" + functionName + "(): parameter not a string: " + param);
        return null;
    }
    return param;
}

function normalizeSelection(pageId, selectionVar) {
    // does such page id exist?
    if (!CCDB.SI.sessionInfo[pageId]) return;
    if (!selectionVar) selectionVar = "selections";
    if (selectionVar.constructor !== String) {
        console.warn("addSelectedItem(): selectionVar not a string: " + selectionVar);
        return;
    }
    if (!CCDB.SI.sessionInfo[pageId][selectionVar] 
            || (CCDB.SI.sessionInfo[pageId][selectionVar].constructor !== Array)) {
        CCDB.SI.sessionInfo[pageId][selectionVar] = [];
        return;
    }
}

/**
 * @returns the id for the current page, or empty if this page does not have the session info stored.
 */
function getPageId() {
    var pageName = window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1);
    if ((pageName == "") || (pageName == "/")) {
        return "hierarchies";
    }
    // does the current have the id defined?
    if (!CCDB.SI.pageNameToPageId[pageName]) return "";
    return CCDB.SI.pageNameToPageId[pageName].id;
}

function getTableWidgetVarName(pageId) {
    for (pageName in CCDB.SI.pageNameToPageId) {
        var id = CCDB.SI.pageNameToPageId[pageName].id;
        if (id == pageId) return CCDB.SI.pageNameToPageId[pageName].tableWidgetVarName;
    }
    return "";
}

function getTabWidgetVarName(pageId) {
    for (pageName in CCDB.SI.pageNameToPageId) {
        var id = CCDB.SI.pageNameToPageId[pageName].id;
        if (id == pageId) return CCDB.SI.pageNameToPageId[pageName].tabWidgetVarName;
    }
    return "";
}

function addSelectedItem(pageId, elementId, selectionVar, lastSelectedVar) {
    if (!verifyId(pageId)) return;
    if (selectionVar && !lastSelectedVar) {
        log.error("addSelectedItem(): parameters 'selectionVar' and 'lastSelectedVar' must both be defined or obth undefined.");
        return;
    }
    if (elementId.constructor !== String) {
        console.warn("addSelectedItem(): elementId not a string: " + elementId);
        return;
    }
    selectionVar = stringParamDefined(selectionVar, "selections", "addSelectedItem");
    if (selectionVar == null) return; // There was an error
    lastSelectedVar = stringParamDefined(lastSelectedVar, "lastSelected", "addSelectedItem");
    if (lastSelectedVar == null) return; // There was an error
    normalizeSelection(pageId, selectionVar);
    if (CCDB.SI.sessionInfo[pageId][selectionVar].indexOf(elementId) < 0) {
        CCDB.SI.sessionInfo[pageId][selectionVar].push(elementId);
    }
    CCDB.SI.sessionInfo[pageId][lastSelectedVar] = elementId;
    storeSessionInfo();
}

function clearSelectedItems(pageId, selectionVar, lastSelectedVar) {
    if (!verifyId(pageId)) return;
    if (selectionVar && !lastSelectedVar) {
        log.error("clearSelectedItems(): parameters 'selectionVar' and 'lastSelectedVar' must both be defined or obth undefined.");
        return;
    }
    selectionVar = stringParamDefined(selectionVar, "selections", "clearSelectedItems");
    if (selectionVar == null) return; // There was an error
    lastSelectedVar = stringParamDefined(lastSelectedVar, "lastSelected", "clearSelectedItems");
    if (lastSelectedVar == null) return; // There was an error
    CCDB.SI.sessionInfo[pageId][selectionVar] = [];
    CCDB.SI.sessionInfo[pageId][lastSelectedVar] = "";
    storeSessionInfo();
}

function removeSelectedItem(pageId, elementId, selectionVar, lastSelectedVar) {
    if (!verifyId(pageId)) return;
    if (selectionVar && !lastSelectedVar) {
        log.error("removeSelectedItem(): parameters 'selectionVar' and 'lastSelectedVar' must both be defined or obth undefined.");
        return;
    }
    if (elementId.constructor !== String) {
        console.warn("removeSelectedItem(): elementId not a string: " + elementId);
        return;
    }
    selectionVar = stringParamDefined(selectionVar, "selections", "removeSelectedItem");
    if (selectionVar == null) return; // There was an error
    lastSelectedVar = stringParamDefined(lastSelectedVar, "lastSelected", "removeSelectedItem");
    if (lastSelectedVar == null) return; // There was an error
    normalizeSelection(pageId, selectionVar);
    var index = CCDB.SI.sessionInfo[pageId][selectionVar].indexOf(elementId);
    if (index > -1) {
        CCDB.SI.sessionInfo[pageId][selectionVar].splice(index, 1);
        if (CCDB.SI.sessionInfo[CCDB.SI.pageId][lastSelectedVar] == elementId) {
            CCDB.SI.sessionInfo[CCDB.SI.pageId][lastSelectedVar] = "";
        }
    } else {
        console.warn("removeSelectedItem(): No such element: " + elementId);
    }
    storeSessionInfo();
}

function isNewSession() {
    return (CCDB.SI.currentSessionId != CCDB.SI.sessionInfo.oldSessionId);
}

function copyTableSelection(tableWidgetVarName, sessionSelectionTable, lastSelectedVar, prevSelectionVar) {
    sessionSelectionTable = stringParamDefined(sessionSelectionTable, "selections", "copyTableSelection");
    if (sessionSelectionTable == null) return; // There was an error
    lastSelectedVar = stringParamDefined(lastSelectedVar, "lastSelected", "copyTableSelection");
    if (lastSelectedVar == null) return; // There was an error
    prevSelectionVar = stringParamDefined(prevSelectionVar, "lastTableSelection", "copyTableSelection");
    if (prevSelectionVar == null) return; // There was an error
    CCDB.SI.sessionInfo[CCDB.SI.pageId][sessionSelectionTable] = PF(tableWidgetVarName).selection.slice();
    var newSelection = CCDB.SI.sessionInfo[CCDB.SI.pageId][sessionSelectionTable];
    if (CCDB.SI.tableAction == "Select") {
        var newSelectedElement = false;
        for (i = newSelection.length - 1; i >= 0; i--) {
            if (CCDB.SI[prevSelectionVar].indexOf(newSelection[i]) < 0) {
                CCDB.SI.sessionInfo[CCDB.SI.pageId][lastSelectedVar] = newSelection[i];
                newSelectedElement = true;
                break;
            }
        }
        if (!newSelectedElement) {
            console.warn("copyTableSelection(): no new selected element detected: old ~ " 
                    + CCDB.SI[prevSelectionVar].toString() + ", new ~ " + newSelection);
        }
    } else if ((CCDB.SI.tableAction == "Deselect") && (CCDB.SI.sessionInfo[CCDB.SI.pageId][sessionSelectionTable] != "")) {
        // has the last selected element been removed?
        if (newSelection.indexOf(CCDB.SI.sessionInfo[CCDB.SI.pageId][lastSelectedVar]) < 0) {
            CCDB.SI.sessionInfo[CCDB.SI.pageId][lastSelectedVar] = "";
        }
    }
    CCDB.SI[prevSelectionVar] = newSelection.slice();
    CCDB.SI.tableAction == "None";
    storeSessionInfo();
}

function saveTabSelection(tabWidgetVarName, sessionSelectionTabs) {
    sessionSelectionTabs = stringParamDefined(sessionSelectionTabs, "tabSelection", "saveTabSelection");
    if (sessionSelectionTabs == null) return; // There was an error
    CCDB.SI.sessionInfo[CCDB.SI.pageId][sessionSelectionTabs] = PF(tabWidgetVarName).getActiveIndex();
    storeSessionInfo();
}

/**
 * Callback for 'rowSelect' onstart event.
 * @param tableWidgetVarName the name of the table widget. Not used at the moment.
 * @returns nothing
 */
function tablePreSelect(tableWidgetVarName) {
    CCDB.SI.tableAction = "Select";
}

/**
 * Callback for 'rowUnselect' onstart event.
 * @param tableWidgetVarName the name of the table widget. Not used at the moment.
 * @returns nothing
 */
function tablePreDeselect(tableWidgetVarName) {
    CCDB.SI.tableAction = "Deselect";
}

function bootstrapDataLoader2(dataKey, tableVarName) {
    if (CCDB.config.jumpToElementOnLoad == true) {
        CCDB.config.selectionInProgress = true;
        CCDB.config.jumpToElementOnLoad = false;
        // store the parameters
        CCDB.dataLoaderInternal = {
                "dataKey" : dataKey,
                "tableVarName" : tableVarName
        }
        jQuery(document).on("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler2);
    }
}

/**
 * selectEntityInTable2 only works on page load, so it can be used for navigating to an entity form another screen.
 * It is used by the o
 * @param dataKey the database ID of the entity
 * @param tableVarName the name of the PrimeFaces table variable
 * @param force if defined and true, the method can be used even if the page has not been loaded
 */
function selectEntityInTable2(dataKey, tableVarName, force) {
    if ((typeof force == 'boolean') && (force == true)) CCDB.config.jumpToElementOnLoad = true;
    bootstrapDataLoader2(dataKey, tableVarName);
    var tableWidget = PF(tableVarName);
    // check whether the data is here. If not, wait a little...
    if ((CCDB.config.tableHasContent < 3) && (tableWidget.jq.find('tr.ui-widget-content').length <= 0)) {
        CCDB.config.tableHasContent++;
        setTimeout(function() {selectEntityInTable2(dataKey, tableVarName, false);}, 300);
        return;
    }

    // table data loaded
    CCDB.config.tableHasContent = -1;
    // search for entity
    var rowRef = tableWidget.jq.find("tr[data-rk='" + dataKey + "']");

    if (rowRef.length > 0) {
        // entity found
        CCDB.config.selectionInProgress = false;
        jQuery(document).off("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler2);
        tableWidget.selectRow(rowRef);
    } else if (!tableWidget.allLoadedLiveScroll) {
        // not found, but there's more data
        tableWidget.loadLiveRows();
    } else {
        // entity not found
        CCDB.config.selectionInProgress = false;
        jQuery(document).off("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler2);
    }
}

/**
 * https://stackoverflow.com/a/901144/3989524
 * @param name the name if the parameter
 * @param url [optional] the url to parse
 * @returns string value o the param if given, empty value if no value give, 'null' if parameter does not exist.
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * This functions selects all elements that are stored in the sessioInfo cookie, or
 * selects the element indicated by the query parameter ID. Whether such an element 
 * exists must first checked by the back-end.
 * 
 * @returns nothing
 */
function selectElementsStoredInSession() {
    if (CCDB.SI.pageId == "" || CCDB.SI.pageId == "log") return;
    if (CCDB.SI.pageId == "hierarchies"){
        selectElementsStoredInSessionForHiearchies();
        return;
        var sessionInfo = CCDB.SI.sessionInfo[CCDB.SI.pageId];
    }
    // bootstrap the selection process
    if (CCDB.SI.jsSelectionLeft === undefined) {
        // store cookie info if required for later
        CCDB.SI.jsSelectionLeft = CCDB.SI.sessionInfo[CCDB.SI.pageId].selections.slice();  // copy the table
        CCDB.SI.jsSelectLast = CCDB.SI.sessionInfo[CCDB.SI.pageId].lastSelected;
        CCDB.SI.jsSelectActionRequired = false;
        CCDB.config.jumpToElementOnLoad = (CCDB.SI.jsSelectionLeft.length > 0);
        // but maybe the id is defined!!!!
        var entityId = getParameterByName("id");
        CCDB.SI.checkedId = null;
        if (entityId) {
            CCDB.SI.queryStringId = entityId;
            CCDB.SI.idCheckInProgress = 0;      // we will start the back-end id check
        } else {
            CCDB.SI.idCheckInProgress = -1;     // check finished or not necessary
        }
    }
    // Now wait for the back-end verification if necessary
    if (CCDB.SI.idCheckInProgress >= 0) {
        if ((CCDB.SI.idCheckInProgress > 5) || (CCDB.SI.checkedId != null)) {
            CCDB.SI.idCheckInProgress = -1;     // check finished or not necessary
        } else {
            CCDB.SI.idCheckInProgress++; // more checks necessary
            setTimeout(selectElementsStoredInSession, 100);
            return;
        }
        if (CCDB.SI.queryStringId == CCDB.SI.checkedId) {
            CCDB.SI.sessionInfo[CCDB.SI.pageId].selections = [CCDB.SI.checkedId];
            CCDB.SI.sessionInfo[CCDB.SI.pageId].lastSelected = CCDB.SI.checkedId;
            CCDB.SI.jsSelectionLeft = [CCDB.SI.checkedId];
            CCDB.SI.jsSelectLast = CCDB.SI.checkedId;
            CCDB.config.jumpToElementOnLoad = true;

        }
    }
    // If the code is at this point, then:
    //     - no back-end check was necessary
    //     - the check finished successfully
    //     - the check timed-out
    //     - or is already selecting the elements 
    if (!CCDB.config.jumpToElementOnLoad 
            && !CCDB.SI.jsSelectActionRequired
            && ((CCDB.SI.jsSelectionLeft === undefined)
                    || (CCDB.SI.jsSelectionLeft == null)
                    || (CCDB.SI.jsSelectionLeft.constructor !== Array)
                    || (CCDB.SI.jsSelectionLeft.length < 1))) return;   // NOTHING TO DO!!!!

    if (!CCDB.config.selectionInProgress) {
        // no data loading
        // let's check if there is more data to be selected...
        var tableWidgetVarName = getTableWidgetVarName(CCDB.SI.pageId);
        if (CCDB.SI.jsSelectionLeft.length > 0) {
            CCDB.SI.jsSelectActionRequired = true;
            // select more
            var elementToSelect = CCDB.SI.jsSelectionLeft.shift();
            selectEntityInTable2(elementToSelect, tableWidgetVarName, true);
        } else {
            // jump to whatever was selected last
            scrollSelectedIntoView(PF(tableWidgetVarName), CCDB.SI.jsSelectLast);
            // all done!!!
            return;
        }
    }
    setTimeout(selectElementsStoredInSession, 100);
}


function selectTabStoredInSession() {
    if (CCDB.SI.pageId == "hierarchies") {
        sessionSelectionTabs = "tabSelection";
        var tabWidgetVarName = getTabWidgetVarName(CCDB.SI.pageId);
        PF(tabWidgetVarName).select(CCDB.SI.sessionInfo[CCDB.SI.pageId][sessionSelectionTabs]);
    }
}

function saveFilter() {
    if (CCDB.SI.pageId == "hierarchies") {
        CCDB.SI.sessionInfo[CCDB.SI.pageId].filter = $("#hierarchies\\:hierarchyTabs\\:filterContainsTree")[0].value
    } else {
        $('th :input').each(function(i, filter){
            if (filter.id.includes(PF(getTableWidgetVarName(CCDB.SI.pageId)).id)){
                if (CCDB.SI.sessionInfo[CCDB.SI.pageId].filter == null){ // compatibility with old cookie that does not have filter defined yet.
                    CCDB.SI.sessionInfo[CCDB.SI.pageId].filter = {};
                } 
                CCDB.SI.sessionInfo[CCDB.SI.pageId].filter[filter.id.split(":")[2]] = filter.value;
            }
          });
    }
    storeSessionInfo();
}

function applyFilterStoredInSession(){
    if (CCDB.SI.pageId == "hierarchies") {
        $("#hierarchies\\:hierarchyTabs\\:filterContainsTree").val(CCDB.SI.sessionInfo[CCDB.SI.pageId].filter);
        filterHiearchies();
    } else {    
            for (key in CCDB.SI.sessionInfo[CCDB.SI.pageId].filter){
                $('input[id*="' + key + '"]').val(CCDB.SI.sessionInfo[CCDB.SI.pageId].filter[key]);
            }
            PF(getTableWidgetVarName(CCDB.SI.pageId)).filter();
    }
}

function copyHiearchiesSelection(xhr, status, args){
    //Call on remoteCommand in home.xhtml
    var selectedIds = args.selectedIds;
    CCDB.SI.sessionInfo[CCDB.SI.pageId].selections = selectedIds
    storeSessionInfo();
}

function selectElementsStoredInSessionForHiearchies(){
    var parameters = [];
    
    ids = CCDB.SI.sessionInfo[CCDB.SI.pageId].selections.replace("[","").replace("]","").split(",")
    for (i = 0; i < ids.length; i++){
        parameters.push({name:'selections', value:ids[i] });
    }
    cookieSelect(parameters);
}

function idCheckResult(result) {
    CCDB.SI.checkedId = result;
}

function clearCookie(){
    CCDB.SI.sessionInfo = emptySessionInfo;
    storeSessionInfo();
}

function copyHiearchiesExpanded(xhr, status, args){
    //Call on remoteCommand in home.xhtml
    var expandedIds = args.expandedIds;
    CCDB.SI.sessionInfo[CCDB.SI.pageId].expanded = expandedIds
    storeSessionInfo();
}

function expandElementsStoredInSession(){
    if (CCDB.SI.pageId == "hierarchies") {
            var parameters = [];
            
            ids = CCDB.SI.sessionInfo[CCDB.SI.pageId].expanded.replace("[","").replace("]","").split(",")
            for (i = 0; i < ids.length; i++){
                parameters.push({name:'expandedIds', value:ids[i] });
            }
            cookieExpand(parameters);
    }
}
