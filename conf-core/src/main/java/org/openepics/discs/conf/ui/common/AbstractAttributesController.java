/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.common;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.openepics.discs.conf.ejb.DAO;
import org.openepics.discs.conf.ejb.DataTypeEJB;
import org.openepics.discs.conf.ejb.TagEJB;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.EntityWithProperties;
import org.openepics.discs.conf.ent.EntityWithTags;
import org.openepics.discs.conf.ent.NamedEntity;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.Tag;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.Summary;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.PropertyValueNotUniqueException;
import org.openepics.discs.conf.util.UnhandledCaseException;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.views.EntityAttrArtifactView;
import org.openepics.discs.conf.views.EntityAttrPropertyValueView;
import org.openepics.discs.conf.views.EntityAttrTagView;
import org.openepics.discs.conf.views.EntityAttributeView;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.views.EntityAttrExternalLinkView;

/**
 * Parent class for all classes that handle entity attributes manipulation
 *
 * @param <C> There are 3 entities in the database having Property values, Tags
 * and Artifacts with the same interface.
 * @param <T> There are 4 property value tables in the database, but all have
 * the same columns and interface.
 * @param <S> There are 5 artifact value tables in the database, but all have
 * the same columns and interface.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public abstract class AbstractAttributesController<C extends ConfigurationEntity & NamedEntity & EntityWithTags &
    EntityWithArtifacts, T extends PropertyValue, S extends Artifact> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected TagEJB tagEJB;
    @Inject
    protected DataTypeEJB dataTypeEJB;

    private List<String> tagsForAutocomplete;

    protected List<EntityAttributeView<C>> attributes;
    protected List<EntityAttributeView<C>> filteredAttributes;
    protected List<EntityAttributeView<C>> selectedAttributes;
    protected List<EntityAttributeView<C>> nonDeletableAttributes;
    private List<EntityAttributeView<C>> filteredDialogAttributes;
    private final List<SelectItem> attributeKinds = UiUtility.buildAttributeKinds();

    protected EntityAttributeView<C> dialogAttribute;
    protected EntityAttrArtifactView<C> downloadArtifactView;
    protected EntityAttrExternalLinkView<C> externalLinkView;

    protected OverlayValueProvider overlay = new OverlayValueProvider();

    private DAO<C> dao;

    /**
     * This method resets the fields related to the dialog that was just shown
     * to the user.
     */
    public void resetFields() {
        dialogAttribute = null;
    }

    /**
     * Adds or modifies an artifact
     *
     * @param event the PrimeFaces provided event
     */
    public void modifyArtifact(ActionEvent event) {

        final EntityAttrArtifactView<C> artifactView = getDialogAttrArtifact();

        if (isArtifactNameUnique(artifactView)) {

            if (artifactView.isArtifactBeingModified()) {
                dao.saveChild(artifactView.getEntity());
            } else {
                dao.addChild(artifactView.getEntity());
            }

            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.ARTIFACT, artifactView.getArtifactName(),
                    artifactView.isArtifactBeingModified() ? Action.UPDATE : Action.CREATE);
            refreshParentEntity(dialogAttribute);
            resetFields();
            clearRelatedAttributeInformation();
            populateAttributesList();
        } else {
            final String formName = getFormId(event.getComponent());
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(formName + "nameValidationMessage", new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, Summary.ERROR.toString(),
                    "File name must be unique."));
        }
    }

    public void modifyExternalLink(ActionEvent event) {
        final EntityAttrExternalLinkView<C> currentExternalLinkView = getDialogExternalLink();
        Set<ExternalLink> existingExternalLinks = ((EntityWithExternalLinks) getSelectedEntity()).getExternalLinkList();
        ExternalLink newOrModifiedExternalLink = currentExternalLinkView.getEntity();

        if (isExternalLinkNameUnique(existingExternalLinks, newOrModifiedExternalLink)) {

            if (currentExternalLinkView.isExternalLinkBeingModified()) {
                dao.updateExternalLink((EntityWithExternalLinks) getSelectedEntity(), newOrModifiedExternalLink);
            } else {
                dao.addExternalLink((EntityWithExternalLinks) getSelectedEntity(), newOrModifiedExternalLink);
            }

            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.EXTERNAL_LINK, newOrModifiedExternalLink.getName(),
                    currentExternalLinkView.isExternalLinkBeingModified() ? Action.UPDATE : Action.CREATE);
            refreshParentEntity(currentExternalLinkView);
            resetFields();
            clearRelatedAttributeInformation();
            populateAttributesList();

        } else {
            final String formName = getFormId(event.getComponent());
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(formName + "nameValidationMessage", new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, Summary.ERROR.toString(),
                    "External link name must be unique."));
        }
    }

    private boolean isArtifactNameUnique(final EntityAttrArtifactView<C> artifactView) {
        for (final Artifact existingArtifact : artifactView.getParentEntity().getEntityArtifactList()) {
            if (!existingArtifact.getId().equals(artifactView.getEntity().getId())
                    && existingArtifact.getName().equals(artifactView.getEntity().getName())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the name of the {@link ExternalLink} as specified by the user
     * is unique. The algorithm must be able to deal with the following use
     * cases:
     * <ol>
     * <li>User adds a new {@link ExternalLink}</li>
     * <li>User modifies an existing {@link ExternalLink} without changing its
     * name</li>
     * <li>User modifies an existing {@link ExternalLink} including an update to
     * the name</li>
     * </ol>
     * In all the above use cases this method must determine of the name of the
     * {@link ExternalLink} is unique among the list of {@link ExternalLink}s
     * associated by the parent entity.
     *
     * This method is made static to facilitate unit testing.
     *
     * @param existingExternalLinks The list of {@link ExternalLink}s already
     * present with the parent.
     * @param newOrModifiedExternalLink The new or modified {@link ExternalLink}
     * that is added or edited.
     * @return <code>true</code> if the name of the {@link ExternalLink} is
     * unique and hence allowed, <code>false</code> otherwise.
     */
    protected static boolean isExternalLinkNameUnique(
            final Set<ExternalLink> existingExternalLinks, final ExternalLink newOrModifiedExternalLink) {

        for (ExternalLink existingExternalLink : existingExternalLinks) {
            if (existingExternalLink.getName().equals(newOrModifiedExternalLink.getName())) {
                if (existingExternalLink.getId().equals(newOrModifiedExternalLink.getId())) {
                    continue;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    protected String getFormId(final UIComponent input) {
        UIComponent cmp = input;
        while ((cmp != null) && !(cmp instanceof UIForm)) {
            cmp = cmp.getParent();
        }
        return cmp != null ? cmp.getId() + ":" : "";
    }

    /**
     * Adds new {@link Tag} to parent {@link ConfigurationEntity}
     */
    public void addNewTag() {
        try {
            final EntityAttrTagView<C> tagView = getDialogAttrTag();
            Tag tag = tagEJB.findById(tagView.getTag());
            if (tag == null) {
                tag = tagView.getEntity();
            }

            C ent = tagView.getParentEntity();
            final Set<Tag> existingTags = ent.getTags();
            if (!existingTags.contains(tag)) {
                existingTags.add(tag);
                dao.save(ent);
                UiUtility.showUniformMessage(Summary.SUCCESS, Entity.TAG, tag.getName(), Action.CREATE);
            }
        } finally {
            refreshParentEntity(dialogAttribute);
            resetFields();
            clearRelatedAttributeInformation();
            populateAttributesList();
        }
    }

    /**
     * The method builds a list of units that are already used. If the list is
     * not empty, it is displayed to the user and the user is prevented from
     * deleting them.
     */
    public void checkAttributesForDeletion() {
        Preconditions.checkNotNull(selectedAttributes);
        Preconditions.checkState(!selectedAttributes.isEmpty());

        filteredDialogAttributes = Lists.newArrayList();
        nonDeletableAttributes = Lists.newArrayList();
        for (final EntityAttributeView<C> attrToDelete : selectedAttributes) {
            if (!canDelete(attrToDelete)) {
                nonDeletableAttributes.add(attrToDelete);
            }
        }
    }

    /**
     * Deletes selected attributes from parent {@link ConfigurationEntity}.
     * These attributes can be {@link Tag},
     * {@link PropertyValue}, {@link Artifact} or {@link ExternalLink}
     *
     * @throws IOException attribute deletion failure
     */
    @SuppressWarnings("unchecked")
    public void deleteAttributes() throws IOException {
        Preconditions.checkNotNull(selectedAttributes);
        Preconditions.checkState(!selectedAttributes.isEmpty());
        Preconditions.checkNotNull(dao);
        Preconditions.checkNotNull(nonDeletableAttributes);
        Preconditions.checkState(nonDeletableAttributes.isEmpty());

        int deletedAttributes = 0;
        for (final EntityAttributeView<C> attributeToDelete : selectedAttributes) {
            if (attributeToDelete instanceof EntityAttrPropertyValueView<?>) {
                deletePropertyValue((T) attributeToDelete.getEntity());
            } else if (attributeToDelete instanceof EntityAttrArtifactView<?>) {
                deleteArtifact((Artifact) attributeToDelete.getEntity());
            } else if (attributeToDelete instanceof EntityAttrTagView<?>) {
                final Tag tagAttr = (Tag) attributeToDelete.getEntity();
                deleteTagFromParent(attributeToDelete.getParentEntity(), tagAttr);
            } else if (attributeToDelete instanceof EntityAttrExternalLinkView<?>) {
                deleteExternalLink((ExternalLink) attributeToDelete.getEntity());
            } else {
                throw new UnhandledCaseException();
            }

            ++deletedAttributes;
        }

        nonDeletableAttributes = null;
        refreshParentEntity(dialogAttribute);
        clearRelatedAttributeInformation();
        populateAttributesList();
        UiUtility.showUniformMessage(Summary.SUCCESS, Entity.PROPERTIES, Action.DELETE, deletedAttributes);
    }

    /**
     * A callback that gives the descendants opportunity to refresh the entity
     * that is related to the attribute that was just processed.
     *
     * @param attributeView the attributeView
     */
    protected void refreshParentEntity(final EntityAttributeView<C> attributeView) {
        // no refresh by default
    }

    protected void deletePropertyValue(final T propValueToDelete) {
        if (!isPropertyValueInherited(propValueToDelete)) {
            dao.deleteChild(propValueToDelete);
        } else {
            propValueToDelete.setPropValue(null);
            dao.saveChild(propValueToDelete);
        }
    }

    private void deleteArtifact(final Artifact artifactToDelete) {
        dao.deleteChild(artifactToDelete);
    }

    private void deleteExternalLink(final ExternalLink externalLink) {
        dao.deleteExternalLink((EntityWithExternalLinks) getSelectedEntity(),
                externalLink);
    }

    /**
     * The main method that prepares the fields for any of the following
     * dialogs:
     * <ul>
     * <li>the dialog to modify a property value</li>
     * <li>the dialog to modify an artifact data</li>
     * </ul>
     */
    public void prepareModifyPropertyPopUp() {
        Preconditions.checkNotNull(selectedAttributes);
        Preconditions.checkState(selectedAttributes.size() == 1);
        dialogAttribute = selectedAttributes.get(0);

        if (getDialogAttrPropertyValue() != null) {
            final EntityAttrPropertyValueView<C> propertyValueView = getDialogAttrPropertyValue();
            PropertyValue propertyValue = propertyValueView.getEntity();
            propertyValueView.setPropertyNameChangeDisabled(propertyValue instanceof DevicePropertyValue
                    || propertyValue instanceof SlotPropertyValue || isPropertyValueInherited(propertyValue));
            propertyNameChangeOverride(propertyValueView);
            filterProperties();

            PrimeFaces.current().ajax().update("modifyPropertyValueForm:modifyPropertyValue");
            PrimeFaces.current().executeScript("PF('modifyPropertyValue').show();");
        }

        if (getDialogAttrArtifact() != null) {
            PrimeFaces.current().ajax().update("modifyArtifactForm:modifyArtifact");
            PrimeFaces.current().executeScript("PF('modifyArtifact').show();");
        } else if (getDialogExternalLink() != null) {
            PrimeFaces.current().ajax().update("modifyArtifactForm:modifyExternalLink");
            PrimeFaces.current().executeScript("PF('modifyExternalLink').show();");
        }
    }

    /**
     * This method is called after the {@link #prepareModifyPropertyPopUp()}
     * calculates the <code>isPropertyNameChangeDisabled</code> value and can be
     * used to override it from the descendant classes.
     *
     * @param propertyValueView the attribute (property value) for which this
     * callback is called
     */
    protected void propertyNameChangeOverride(final EntityAttrPropertyValueView<C> propertyValueView) {
        // no override by default
    }

    /**
     * Modifies {@link PropertyValue}
     *
     * @param event the event
     */
    public void modifyPropertyValue(ActionEvent event) {
        Preconditions.checkNotNull(dialogAttribute);
        Preconditions.checkState(isSingleAttributeSelected());

        boolean reportedError = false;
        try {
            dao.saveChild(dialogAttribute.getEntity());
            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.PROPERTY, dialogAttribute.getName(), Action.UPDATE);
        } catch (EJBException e) {
            if (UiUtility.causedBySpecifiedExceptionClass(e, PropertyValueNotUniqueException.class)) {
                reportedError = true;
                final String formName = getFormId(event.getComponent());
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage(formName + "propertyValidationMessage", new FacesMessage(
                        FacesMessage.SEVERITY_ERROR, Summary.ERROR.toString(), "Value must be unique."));
            } else {
                throw e;
            }
        } finally {
            if (!reportedError) {
                refreshParentEntity(dialogAttribute);
                resetFields();
                clearRelatedAttributeInformation();
                populateAttributesList();
            }
        }
    }

    /**
     * Finds artifact file that was uploaded on the file system and returns it
     * to be downloaded
     *
     * @return Artifact file to be downloaded
     * @throws FileNotFoundException Thrown if file was not found on file system
     */
    @SuppressWarnings("unchecked")
    public StreamedContent getDownloadFile() throws FileNotFoundException {
        final S selectedArtifact = (S) downloadArtifactView.getEntity();
        // guess mime type based on the original file name, not on the name of the blob (UUID).
        final String contentType = FacesContext.getCurrentInstance().getExternalContext()
                .getMimeType(selectedArtifact.getName());

        byte[] content = selectedArtifact.getBinaryData().getContent();

        // The null check is needed as some uploaded artifacts were missing from the file system
        // during migration of artifacts from file system to blobs.
        if (content == null) {
            throw new FileNotFoundException("Artifact content is missing.");
        }

        return new DefaultStreamedContent(new ByteArrayInputStream(content), contentType, selectedArtifact.getName());
    }

    public EntityAttributeView<C> getDownloadArtifact() {
        return downloadArtifactView;
    }

    public void setDownloadArtifact(EntityAttrArtifactView<C> downloadArtifact) {
        this.downloadArtifactView = downloadArtifact;
    }

    public void setExternalLinkView(EntityAttrExternalLinkView<C> externalLinkView) {
        this.externalLinkView = externalLinkView;
    }

    /**
     * This method determines whether the entity attribute should have the
     * "pencil" icon displayed in the UI.
     *
     * @param attributeView The object containing the UI info for the attribute
     * table row.
     * @return <code>true</code> if the attribute can be edited,
     * <code>false</code> otherwise.
     */
    public abstract boolean canEdit(EntityAttributeView<C> attributeView);

    /**
     * This method determines whether the entity attribute can be deleted - is
     * not used anywhere.
     *
     * @param attributeView The object containing the UI info for the attribute
     * table row.
     * @return <code>true</code> if the attribute can be deleted,
     * <code>false</code> otherwise.
     */
    protected abstract boolean canDelete(EntityAttributeView<C> attributeView);

    private boolean isPropertyValueInherited(PropertyValue propValue) {
        List<ComptypePropertyValue> parentProperties = null;
        EntityWithProperties parentEntity = propValue.getPropertiesParent();
        if (parentEntity != null) {
            if (parentEntity instanceof Slot) {
                if (((Slot) parentEntity).isHostingSlot()) {
                    parentProperties = ((Slot) parentEntity).getComponentType().getComptypePropertyList();
                } else {
                    return false;
                }
            } else if (parentEntity instanceof Device) {
                parentProperties = ((Device) parentEntity).getComponentType().getComptypePropertyList();
            } else if (parentEntity instanceof ComponentType) {
                return false;
            } else {
                throw new UnhandledCaseException();
            }
        }

        if (Utility.isNullOrEmpty(parentProperties)) {
            return false;
        }

        final String propertyName = propValue.getProperty().getName();
        for (final ComptypePropertyValue inheritedPropVal : parentProperties) {
            if (inheritedPropVal.isPropertyDefinition()
                    && ((inheritedPropVal.isDefinitionTargetDevice() && parentEntity instanceof Device)
                    || (inheritedPropVal.isDefinitionTargetSlot() && parentEntity instanceof Slot))
                    && propertyName.equals(inheritedPropVal.getProperty().getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method is called when there is no main entity selected and the
     * attributes tables must be made empty
     */
    public void clearRelatedAttributeInformation() {
        attributes = null;
        filteredAttributes = null;
        selectedAttributes = null;
    }

    protected abstract C getSelectedEntity();

    protected abstract T newPropertyValue();

    protected abstract Artifact newArtifact();

    protected void deleteTagFromParent(C parent, Tag tag) {
        Preconditions.checkNotNull(parent);
        parent.getTags().remove(tag);
        dao.save(parent);
    }

    /**
     * Filters a list of possible properties to attach to the entity based on
     * the association type.
     */
    protected abstract void filterProperties();

    public void populateAttributesList() {
        populateAttributesList(null);
    }

    /**
     * Populates attributes specified by id. If id is null, populates all
     * attributes.
     *
     * @param ids the id of the attribute
     */
    public abstract void populateAttributesList(Long ids);

    private void fillTagsAutocomplete() {
        tagsForAutocomplete = tagEJB.findAllSorted().stream().map(Tag::getName).collect(Collectors.toList());
    }

    /**
     * Returns list of all attributes for current {@link ConfigurationEntity}
     *
     * @return the list of attributes
     */
    public List<EntityAttributeView<C>> getAttributes() {
        return attributes;
    }

    /**
     * Prepares the UI data for addition of {@link Tag}
     */
    public void prepareForTagAdd() {
        fillTagsAutocomplete();
        dialogAttribute = new EntityAttrTagView<>(getSelectedEntity());
    }

    /**
     * Prepares the UI data for addition of {@link Artifact}
     */
    public void prepareForArtifactAdd() {
        final Artifact artifact = newArtifact();
        final C selectedEntity = getSelectedEntity();
        artifact.setArtifactsParent(selectedEntity);
        dialogAttribute = new EntityAttrArtifactView<>(artifact, selectedEntity);
    }

    public void prepareForExternalLinkAdd() {
        final ExternalLink externalLink = new ExternalLink();
        final C selectedEntity = getSelectedEntity();
        dialogAttribute = new EntityAttrExternalLinkView<>(externalLink, selectedEntity);
    }

    /**
     * @return the selected table rows (UI view presentation)
     */
    public List<EntityAttributeView<C>> getSelectedAttributes() {
        return selectedAttributes;
    }

    /**
     * @param selectedAttributes a list of property values, tags and artifacts
     */
    public void setSelectedAttributes(List<EntityAttributeView<C>> selectedAttributes) {
        this.selectedAttributes = selectedAttributes;
    }

    protected void setDao(DAO<C> dao) {
        this.dao = dao;
    }

    /**
     * Used by the {@link Tag} input value control to display the list of
     * auto-complete suggestions. The list contains the tags already stored in
     * the database.
     *
     * @param query The text the user typed so far.
     * @return The list of auto-complete suggestions.
     */
    public List<String> tagAutocompleteText(String query) {
        final String queryUpperCase = query.toUpperCase();
        return tagsForAutocomplete.stream().filter(e -> e.toUpperCase().startsWith(queryUpperCase))
                .collect(Collectors.toList());
    }

    /**
     * @return the filteredAttributes
     */
    public List<EntityAttributeView<C>> getFilteredAttributes() {
        return filteredAttributes;
    }

    /**
     * @param filteredAttributes the filteredAttributes to set
     */
    public void setFilteredAttributes(List<EntityAttributeView<C>> filteredAttributes) {
        this.filteredAttributes = filteredAttributes;
    }

    /**
     * @return the attributeKinds
     */
    public List<SelectItem> getAttributeKinds() {
        return attributeKinds;
    }

    /**
     * @return <code>true</code> if a single attribute is selected,
     * <code>false</code> otherwise.
     */
    public boolean isSingleAttributeSelected() {
        return (selectedAttributes != null) && (selectedAttributes.size() == 1);
    }

    /**
     * This method is called from the UI and resets a table with the implicit ID
     * "propertySelect" in the form indicated by the parameter.
     *
     * @param id the ID of the from containing a table #propertySelect
     */
    public void resetPropertySelection(final String id) {
        final DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(id + ":propertySelect");
        dataTable.setSortBy(null);
        dataTable.setFirst(0);
        dataTable.setFilteredValue(null);
        dataTable.setFilters(null);
    }

    /**
     * @return the nonDeletableAttributes
     */
    public List<EntityAttributeView<C>> getNonDeletableAttributes() {
        return nonDeletableAttributes;
    }

    /**
     * @return the filteredDialogAttributes
     */
    public List<EntityAttributeView<C>> getFilteredDialogAttributes() {
        return filteredDialogAttributes;
    }

    /**
     * @param filteredDialogAttributes the filteredDialogAttributes to set
     */
    public void setFilteredDialogAttributes(List<EntityAttributeView<C>> filteredDialogAttributes) {
        this.filteredDialogAttributes = filteredDialogAttributes;
    }

    /**
     * @return the dialogAttribute
     */
    public EntityAttrTagView<C> getDialogAttrTag() {
        if (dialogAttribute instanceof EntityAttrTagView<?>) {
            return (EntityAttrTagView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the dialogAttribute
     */
    public EntityAttrPropertyValueView<C> getDialogAttrPropertyValue() {
        if (dialogAttribute instanceof EntityAttrPropertyValueView<?>) {
            return (EntityAttrPropertyValueView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the dialogAttribute
     */
    public EntityAttrArtifactView<C> getDialogAttrArtifact() {
        if (dialogAttribute instanceof EntityAttrArtifactView<?>) {
            return (EntityAttrArtifactView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the dialogExternalLink
     */
    public EntityAttrExternalLinkView<C> getDialogExternalLink() {
        if (dialogAttribute instanceof EntityAttrExternalLinkView<?>) {
            return (EntityAttrExternalLinkView<C>) dialogAttribute;
        }
        return null;
    }

    /**
     * @return the overlay
     */
    public OverlayValueProvider getOverlay() {
        return overlay;
    }
}
