/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Lists;
import org.openepics.discs.conf.ejb.InstallationEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ejb.SlotRelationEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelation;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.service.SlotRelationship;
import org.openepics.discs.conf.service.SlotRelationshipService;
import org.openepics.discs.conf.service.SlotRelationshipServiceException;
import org.openepics.discs.conf.ui.trees.FilteredTreeNode;
import org.openepics.discs.conf.ui.trees.SlotRelationshipTree;
import org.openepics.discs.conf.ui.util.Action;
import org.openepics.discs.conf.ui.util.ConnectsManager;
import org.openepics.discs.conf.ui.util.Entity;
import org.openepics.discs.conf.ui.util.Summary;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.Utility;
import org.openepics.discs.conf.views.SlotRelationshipView;
import org.openepics.discs.conf.views.SlotView;
import org.primefaces.PrimeFaces;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Named
@ViewScoped
public class RelationshipController implements Serializable {
    private static final long serialVersionUID = 1L;

    @Inject private SlotEJB slotEJB;
    @Inject private InstallationEJB installationEJB;
    @Inject private SlotRelationEJB slotRelationEJB;
    @Inject private SlotPairEJB slotPairEJB;
    @Inject private ConnectsManager connectsManager;
    @Inject private HierarchiesController hierarchiesController;
    @Inject private SlotRelationshipService slotRelationshipService;

    private List<SlotRelationshipView> relationships;
    private List<SlotRelationshipView> filteredRelationships;
    private List<SelectItem> relationshipTypes;

    private List<SlotRelationshipView> selectedRelationships;

    private List<SlotRelationshipView> addEditDialogRelationships;
    private List<String> relationshipTypesForDialog;
    private Map<String, SlotRelation> slotRelationBySlotRelationStringName;

    private SlotRelationshipTree containsTree;
    private RelationshipTreeSelectionModes relationshipTreeSelectionMode;
    private String selectedRelationshipName;

    private List<String> filteredSourceRelationshipNames;
    private List<String> filteredTargetRelationshipNames;

    private boolean editExistingRelationship;

    /**
     * Orders the {@link SlotPair}s according to the relationship type and parent {@link Slot}
     */
    private class SlotPairComparator implements Comparator<SlotPair> {
        private final Slot slot;

        public SlotPairComparator(final Slot slot) {
            this.slot = slot;
        }

        @Override
        public int compare(SlotPair o1, SlotPair o2) {
            if (o1 == null)
                return (o2 == null ? 0 : 1);
            if (o2 == null)
                return -1;
            if (o1.getSlotRelation().getName() == SlotRelationName.CONTAINS) {
                // o1: CONTAINS
                if (o2.getSlotRelation().getName() == SlotRelationName.CONTAINS) {
                    // o2: CONTAINS
                    if (o1.getParentSlot().equals(slot)) {
                        // o1 is "CONTAINS" (not "CONTAINED IN")
                        if (o2.getParentSlot().equals(slot)) {
                            // o2 is also "CONTAINS"
                            return creationDifference(o1, o2);
                        } else {
                            // o2 is "CONTAINED IN", which means o1 comes after o2
                            return 1;
                        }
                    } else {
                        // o1 is "CONTAINED IN"
                        if (o2.getParentSlot().equals(slot)) {
                            // o2 is "CONTAINS"
                            return -1;
                        } else {
                            // o2 is also "CONTAINED IN"
                            return creationDifference(o1, o2);
                        }
                    }
                } else {
                    // only o1 is "CONTAINS", o2 isn't
                    return -1;
                }
            } else if (o2.getSlotRelation().getName()  == SlotRelationName.CONTAINS) {
                // only o2 is "CONTAINS"
                return 1;
            } else {
                // no relationship is "CONTAINS"
                return creationDifference(o1, o2);
            }
        }

        private int creationDifference(final SlotPair o1, final SlotPair o2) {
            return Long.compare(o1.getId(), o2.getId());
        }
    }

    /**
     * Primefaces p:tree select modes.
     */
    private enum RelationshipTreeSelectionModes {
        SINGLE("single"), MULTIPLE("multiple");

        private String value;

        RelationshipTreeSelectionModes(String value) {
            this.value = value;
        }

        /**
         * Returns the string representation of enum. This method is used by the Primefaces framework to get the
         * value for the p:tree's "selectionMode" property.
         *
         * @return the string representation of the enum.
         */
        public String toString() {
            return value;
        }
    }

    public RelationshipController() {}

    /** Java EE post construct life-cycle method. */
    @PostConstruct
    public void init() {
        addEditDialogRelationships = Lists.newArrayList();
        relationshipTypes = buildRelationshipTypeList();
        containsTree = buildContainsTree();
    }

    /** Tell this bean which {@link HierarchiesController} is its "master"
     * @param hierarchiesController the {@link HierarchiesController} master bean
     */
    protected void setUIParent(HierarchiesController hierarchiesController) {
        this.hierarchiesController = hierarchiesController;
    }

    private List<SelectItem> buildRelationshipTypeList() {
        Builder<SelectItem> immutableListBuilder = ImmutableList.builder();
        immutableListBuilder.add(new SelectItem("", "Select one"));

        final List<SlotRelation> slotRelations = slotRelationEJB.findAll();
        slotRelations.sort(new Comparator<SlotRelation>() {
            @Override
            public int compare(SlotRelation o1, SlotRelation o2) {
                return o1.getNameAsString().compareTo(o2.getNameAsString());
            }});
        // LinkedHashMap preserves the ordering. Important for UI.
        slotRelationBySlotRelationStringName = new LinkedHashMap<>();
        for (final SlotRelation slotRelation : slotRelations) {
            immutableListBuilder.add(new SelectItem(slotRelation.getNameAsString(), slotRelation.getNameAsString()));
            immutableListBuilder.add(new SelectItem(slotRelation.getIname(), slotRelation.getIname()));
            slotRelationBySlotRelationStringName.put(slotRelation.getNameAsString(), slotRelation);
            slotRelationBySlotRelationStringName.put(slotRelation.getIname(), slotRelation);
        }
        if (connectsManager.getCableDBStatus()) {
            immutableListBuilder.add(new SelectItem(connectsManager.getRelationshipName(),
                    connectsManager.getRelationshipName()));
        }

        relationshipTypesForDialog = ImmutableList.copyOf(slotRelationBySlotRelationStringName.keySet().iterator());
        return immutableListBuilder.build();
    }

    private SlotRelationshipTree buildContainsTree() {
        final SlotView rootView = new SlotView(slotEJB.getRootNode(), null, 1, slotEJB);
        SlotRelationshipTree relationshipTree =
                new SlotRelationshipTree(SlotRelationName.CONTAINS, slotEJB, installationEJB, slotRelationEJB);
        relationshipTree.setRootNode(new FilteredTreeNode<SlotView>(rootView, null, relationshipTree));
        return relationshipTree;
    }

    /** Add the relationship information connected to one {@link Slot} to the relationship list.
     * @param slot the {@link Slot} to add the information for
     * @param forceInit if <code>true</code> recreate the relationship list even if it already contains some information
     * and add just the one {@link Slot} info to the new list. If <code>false</code> add the relationship information to
     * the already existing list.
     */
    protected void initRelationshipList(final Slot slot, final boolean forceInit) {
        if (forceInit || relationships == null) {
            relationships = Lists.newArrayList();
        }
        addToRelationshipList(slot);
    }

    /** Add the relationship information connected to multiple {@link Slot} objects to the relationship list.
     * @param slots the list of {@link Slot} objects to add the information for
     * @param forceInit if <code>true</code> recreate the relationship list even if it already contains some information
     * and add just the one {@link Slot} info to the new list. If <code>false</code> add the relationship information to
     * the already existing list.
     */
    protected void initRelationshipList(final List<Slot> slots, final boolean forceInit) {
        if (forceInit || relationships == null) {
            relationships = Lists.newArrayList();
        }
        slots.stream().forEach(slot -> addToRelationshipList(slot));
    }

    private void addToRelationshipList(final Slot slot) {
        final Slot rootSlot = slotEJB.getRootNode();
        final SlotPairComparator comparator = new SlotPairComparator(slot);
        final List<SlotPair> slotPairs = slotPairEJB.getSlotRelations(slot);
        slotPairs.sort(comparator);

        for (final SlotPair slotPair : slotPairs) {
            if (!slotPair.getParentSlot().equals(rootSlot)) {
                relationships.add(new SlotRelationshipView(slotPair, slot));
            }
        }

        final List<Slot> connectedSlots = connectsManager.getSlotConnects(slot);
        for (final Slot targetSlot : connectedSlots) {
            relationships.add(new SlotRelationshipView(slot.getId()+"c"+targetSlot.getId(), slot, targetSlot,
                    connectsManager.getRelationshipName()));
        }
    }

    /** Remove the relationships connected to one {@link Slot} from the relationship list and the
     * list of selected relationships if necessary.
     * @param slot the {@link Slot} to remove the information for
     */
    protected void removeRelatedRelationships(final Slot slot) {
        final ListIterator<SlotRelationshipView> relationsList = relationships.listIterator();
        while (relationsList.hasNext()) {
            final SlotRelationshipView relationshipView = relationsList.next();
            if (slot.getName().equals(relationshipView.getSourceSlotName())) {
                relationsList.remove();
            }
        }
        if (selectedRelationships!= null) {
            Iterator<SlotRelationshipView> i = selectedRelationships.iterator();
            while (i.hasNext()) {
                SlotRelationshipView selectedRelationship = i.next();
                if (selectedRelationship.getSourceSlotName().equals(slot.getName()))
                    i.remove();
            }
        }
    }

    /** Called from {@link HierarchiesController} when the user deselects everything. */
    protected void clearRelationshipInformation() {
        relationships = null;
        filteredRelationships = null;
        selectedRelationships = null;
    }

    /**
     * This method gets called when the relationship processing is complete or dialog is simply closed. It properly
     * refreshes the selected node state so that the hierarchy behaves consistently.
     * The currently selected slot is also refreshed, so that new relationship data is displayed.
     */
    public void onRelationshipPopupClose() {
        // clear the previous dialog selection for the next use
        if (!addEditDialogRelationships.isEmpty()) {
            containsTree.getSelectedNodes().stream().forEach(selectedNode -> selectedNode.setSelected(false));
            addEditDialogRelationships.clear();
        }
    }

    /** Called when button to delete relationship is clicked */
    public void onRelationshipDelete() {
        if (canRelationshipBeDeleted()) {
            final List<SlotRelationship> existingRelationships = selectedRelationships.stream()
                    .map(relationshipView -> relationshipView.getSlotRelationship())
                    .collect(Collectors.toList());
            final List<SlotRelationship> deleteRelationships =
                    slotRelationshipService.deleteRelationships(existingRelationships);
            final Set<Long> nodesToRefresh = new HashSet<>();
            for (SlotRelationshipView selectedRelationship : selectedRelationships) {
                relationships.remove(selectedRelationship);
                nodesToRefresh.add(selectedRelationship.getSlotPair().getParentSlot().getId());
                nodesToRefresh.add(selectedRelationship.getSlotPair().getChildSlot().getId());
            }
            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.RELATIONSHIPS, Action.DELETE,
                    deleteRelationships.size());
            hierarchiesController.refreshTrees(nodesToRefresh);
            selectedRelationships = null;
        } else {
            PrimeFaces.current().executeScript("PF('cantDeleteRelation').show();");
        }
    }

    /** Tells the UI whether the relationship "Edit" button can be enabled.
     * @return <code>true</code> if only one relationship is selected and is of correct type,
     * <code>false</code> otherwise.
     */
    public boolean canRelationshipBeEdited() {

        if (selectedRelationships == null || selectedRelationships.isEmpty())
            return false;
        boolean relationshipsWithoutSlotPairExist = selectedRelationships.stream()
                .anyMatch(selectedRelationship -> selectedRelationship.getSlotPair() == null);
        if (relationshipsWithoutSlotPairExist) {
            return false;
        }
        // areAllSelectedRelationshipNamesEqual
        return selectedRelationships.stream()
                .map(selectedRelationship -> selectedRelationship.getRelationshipName())
                .distinct()
                .limit(2)
                .count() <= 1;
    }

    private boolean canRelationshipBeDeleted() {
        if (selectedRelationships == null || selectedRelationships.isEmpty())
            return false;
        for (SlotRelationshipView selectedRelationship : selectedRelationships) {
            if (selectedRelationship.getSlotPair() == null)
                return false;
            if (selectedRelationship.getSlotPair().getSlotRelation().getName().equals(SlotRelationName.CONTAINS)
                && !slotPairEJB.slotHasMoreThanOneContainsRelation(selectedRelationship.getSlotPair().getChildSlot()))
                return false;
        }
        return true;
    }

    /** Prepares data for editing new relationship */
    public void prepareEditRelationshipPopup() {
        Preconditions.checkState((selectedRelationships != null) && (!selectedRelationships.isEmpty()));

        containsTree.unselectAllTreeNodes();
        editExistingRelationship = true;
        // setups the dialog
        relationshipTreeSelectionMode = RelationshipTreeSelectionModes.SINGLE;
        addEditDialogRelationships = selectedRelationships.stream()
                .map(selectedRelationship -> new SlotRelationshipView(selectedRelationship.getSlotPair(),
                        selectedRelationship.getSourceSlot()))
                .collect(Collectors.toList());
        selectedRelationshipName = addEditDialogRelationships.get(0).getRelationshipName();

        // select the target node of the first selected relationship as the selected value for the tree control in the
        // add/edit dialog.
        final FilteredTreeNode<SlotView> node =
                containsTree.findNode(addEditDialogRelationships.get(0).getTargetSlot());
        node.setSelected(true);
        containsTree.setSingleSelectedNode(node);

        // modify relationship types drop down menu
        relationshipTypesForDialog = ImmutableList.copyOf(slotRelationBySlotRelationStringName.keySet().iterator());

        // refresh the tree view for selecting target slot in the edit dialog.
        containsTree = buildContainsTree();
    }

    /** Prepares data for adding new relationship(s) */
    public void prepareAddRelationshipPopup() {
        Preconditions.checkState(!hierarchiesController.getSelectedSlots().isEmpty());
        Preconditions.checkState(hierarchiesController.isAtLeastOneNodeSelected());

        containsTree.unselectAllTreeNodes();
        editExistingRelationship = false;
        relationshipTreeSelectionMode = hierarchiesController.getSelectedSlots().size() == 1 ?
                RelationshipTreeSelectionModes.MULTIPLE :
                RelationshipTreeSelectionModes.SINGLE;
        selectedRelationshipName = null;
        List<Slot> selectedNodeSlots = hierarchiesController.getSelectedSlots();
        // clear the previous dialog selection in case the dialog was already used before
        addEditDialogRelationships = selectedNodeSlots.stream()
                .map(slot -> new SlotRelationshipView(null, slot))
                .collect(Collectors.toList());

        // modify relationship types drop down menu
        boolean areAllHostingSlots = hierarchiesController.getSelectedSlots().stream()
                .allMatch(selectedSlot -> selectedSlot.isHostingSlot());
        if (areAllHostingSlots) {
            relationshipTypesForDialog = ImmutableList.copyOf(slotRelationBySlotRelationStringName.keySet().iterator());
            selectedRelationshipName = SlotRelationName.CONTROLS.toString();
        } else {
            relationshipTypesForDialog = ImmutableList.of(SlotRelationName.CONTAINS.toString(),
                    SlotRelationName.CONTAINS.inverseName());
            selectedRelationshipName = SlotRelationName.CONTAINS.toString();
        }

        // refresh the tree view for selecting target slot in the edit dialog.
        containsTree = buildContainsTree();
    }

    /**
     * Called when user clicks add button to add new relationship. Relationship is added if this does not
     * cause a loop on CONTAINS relationships
     */
    public void onRelationshipAdd() {
        try {
            if (editExistingRelationship) {
                editExistingRelationships();
            } else {
                addNewRelationships();
                clearRelationshipInformation();
                initRelationshipList(hierarchiesController.getSelectedSlots(), true);
            }
        } finally {
            onRelationshipPopupClose();
        }
    }

    private void editExistingRelationships() {
        addEditDialogRelationships.forEach(relationship -> Preconditions.checkNotNull(relationship.getSlotPair()));

        final SlotRelation newSlotRelation = slotRelationBySlotRelationStringName.get(selectedRelationshipName);
        final boolean isContainsAdded = (newSlotRelation.getName() == SlotRelationName.CONTAINS);
        final FilteredTreeNode<SlotView> newTargetNode = containsTree.getSelectedNodes().get(0);

        try {
            final List<SlotRelationship> existingRelationships = addEditDialogRelationships.stream()
                    .map(relationshipView -> relationshipView.getSlotRelationship())
                    .collect(Collectors.toList());
            final Set<Long> nodesToRefresh = new HashSet<>();
            if (isSlotRelationNameInverse(selectedRelationshipName)) {
                // Add old target (i.e. the parent) to the list of nodes to refresh. This way, the child won't be
                // displayed in the old perent's list of children anymore.
                nodesToRefresh.addAll(existingRelationships.stream()
                        .map(existingRelationship -> existingRelationship.getTargetSlot().getId())
                        .collect(Collectors.toList()));
            }
            final List<SlotRelationship> updatedRelationships = slotRelationshipService.updateRelationships(
                    existingRelationships, newTargetNode.getData().getSlot(), newSlotRelation);

            boolean expandSelectedHierarchyNodes = false;
            for (SlotRelationship updatedRelationship : updatedRelationships) {
                final OptionalInt indexOfRelationshipViewToReplace = IntStream.range(0, relationships.size())
                        .filter(i -> relationships.get(i).getId().equals(updatedRelationship.getId()))
                        .findFirst();
                if (indexOfRelationshipViewToReplace.isPresent()) {
                    relationships.set(
                            indexOfRelationshipViewToReplace.getAsInt(),
                            new SlotRelationshipView(updatedRelationship.getSlotPair(),
                                    updatedRelationship.getSourceSlot()));
                }

                nodesToRefresh.add(updatedRelationship.getSourceSlot().getId());
                nodesToRefresh.add(updatedRelationship.getTargetSlot().getId());

                expandSelectedHierarchyNodes = expandSelectedHierarchyNodes ||
                        (isContainsAdded && isSlotSelected(updatedRelationship.getSlotPair().getParentSlot()));
            }

            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.RELATIONSHIP,
                    addEditDialogRelationships.get(0).getRelationshipName(), Action.UPDATE);

            selectedRelationships = null;

            hierarchiesController.refreshTrees(nodesToRefresh);

            if (expandSelectedHierarchyNodes) {
                hierarchiesController.expandSelectedNodes();
            }
        } catch (SlotRelationshipServiceException e) {
            UiUtility.showUniformMessage(Summary.ERROR, Entity.RELATIONSHIP,
                    selectedRelationshipName, Action.UPDATE, e.getMessage());
        }
    }

    private boolean isSlotRelationNameInverse(String slotRelationName) {
        return slotRelationName.equals(SlotRelationName.CONTAINS.inverseName()) ||
                slotRelationName.equals(SlotRelationName.CONTROLS.inverseName()) ||
                slotRelationName.equals(SlotRelationName.POWERS.inverseName());
    }

    private boolean isSlotSelected(Slot slot) {
        for (Slot selectedSlot : hierarchiesController.getSelectedSlots()) {
            if (slot == selectedSlot) {
                return true;
            }
        }
        return false;
    }

    private void addNewRelationships() {
        // we will create new entities in memory and check all of them. Then we will add them in a single transaction
        Preconditions.checkArgument(!containsTree.getSelectedNodes().isEmpty());
        final SlotRelation newSlotRelation =
                slotRelationBySlotRelationStringName.get(selectedRelationshipName);
        final boolean isForwardRelation = newSlotRelation.getNameAsString().equals(selectedRelationshipName);

        try {
            final List<Slot> sourceSlots = addEditDialogRelationships.stream()
                    .map(relationshipView -> relationshipView.getSourceSlot())
                    .collect(Collectors.toList());
            final List<Slot> targetSlots = containsTree.getSelectedNodes().stream()
                    .map(selectedNode -> selectedNode.getData().getSlot())
                    .collect(Collectors.toList());
            final List<SlotRelationship> addedRelationships =
                    slotRelationshipService.createRelationships(sourceSlots, targetSlots, newSlotRelation,
                            isForwardRelation);

            for (SlotRelationshipView dialogRelationship : addEditDialogRelationships) {
                Slot sourceSlot = dialogRelationship.getSourceSlot();
                List<SlotPair> addedRelationshipsForSourceSlot = addedRelationships.stream()
                        .filter(relationship -> relationship.getSourceSlot().equals(sourceSlot.getId()))
                        .map(relationship -> relationship.getSlotPair())
                        .collect(Collectors.toList());
                relationships.addAll(addedRelationshipsForSourceSlot.stream()
                        .map(relationship -> new SlotRelationshipView(relationship, sourceSlot))
                        .collect(Collectors.toList()));
            }

            int numRelationshipsAdded = addedRelationships.size();
            UiUtility.showUniformMessage(Summary.SUCCESS, Entity.RELATIONSHIPS, Action.CREATE, numRelationshipsAdded);

            final Set<Long> sourceNodesToRefresh = addedRelationships.stream()
                    .map(relationship -> relationship.getSourceSlot().getId())
                    .collect(Collectors.toSet());
            final Set<Long> targetNodesToRefresh = containsTree.getSelectedNodes().stream()
                    .map(selectedNode -> selectedNode.getData().getSlot().getId())
                    .collect(Collectors.toSet());
            final Set<Long> nodesToRefresh = Stream.concat(sourceNodesToRefresh.stream(), targetNodesToRefresh.stream())
                    .collect(Collectors.toSet());
            hierarchiesController.refreshTrees(nodesToRefresh);

            // expand tree if nodes gained new CONTAINS child(ren)
            final boolean isContainsAdded = (newSlotRelation.getName() == SlotRelationName.CONTAINS);
            if (isContainsAdded && (numRelationshipsAdded > 0) && isForwardRelation) {
                hierarchiesController.expandSelectedNodes();
            }

            containsTree.unselectAllTreeNodes();
        } catch (SlotRelationshipServiceException e) {
            UiUtility.showUniformMessage(Summary.ERROR, Entity.RELATIONSHIP,
                    selectedRelationshipName, Action.UPDATE, e.getMessage());
        }
    }

    /** Expands the selected node or entire tree */
    public void expandTreeNodes() {
        List<FilteredTreeNode<SlotView>> selectedNodes = containsTree.getSelectedNodes();
        if (Utility.isNullOrEmpty(selectedNodes)) {
            expandOrCollapseNode(containsTree.getRootNode(), true);
        } else {
            selectedNodes.forEach(selectedNode -> expandOrCollapseNode(selectedNode, true));
        }
    }

    /** Collapses the selected node or entire tree */
    public void collapseTreeNodes() {
        List<FilteredTreeNode<SlotView>> selectedNodes = containsTree.getSelectedNodes();
        if (Utility.isNullOrEmpty(selectedNodes)) {
            expandOrCollapseNode(containsTree.getRootNode(), false);
        } else {
            selectedNodes.forEach(selectedNode -> expandOrCollapseNode(selectedNode, false));
        }
    }

    private void expandOrCollapseNode(final FilteredTreeNode<SlotView> parent, final boolean expand) {
        parent.setExpanded(expand);
        for (final  FilteredTreeNode<SlotView> node : parent.getFilteredChildren()) {
            expandOrCollapseNode(node, expand);
        }
    }

    /** @return The list of relationships for the currently selected slot. */
    public List<SlotRelationshipView> getRelationships() {
        return relationships;
    }
    public void setRelationships(List<SlotRelationshipView> relationships) {
        this.relationships = relationships;
    }

    /** @return the {@link List} of relationship types to display in the filter drop down selection. */
    public List<SelectItem> getRelationshipTypes() {
        return relationshipTypes;
    }

    /** @return the filteredRelationships */
    public List<SlotRelationshipView> getFilteredRelationships() {
        return filteredRelationships;
    }
    /** @param filteredRelationships the filteredRelationships to set */
    public void setFilteredRelationships(List<SlotRelationshipView> filteredRelationships) {
        this.filteredRelationships = filteredRelationships;
    }

    /** @return the selectedRelationships */
    public List<SlotRelationshipView> getSelectedRelationships() {
        return selectedRelationships;
    }
    /** @param selectedRelationships the selectedRelationships to set */
    public void setSelectedRelationships(List<SlotRelationshipView> selectedRelationships) {
        this.selectedRelationships = selectedRelationships;
    }

    /** @return the relationshipTypesForDialog */
    public List<String> getRelationshipTypesForDialog() {
        return relationshipTypesForDialog;
    }
    /** @param relationshipTypesForDialog the relationshipTypesForDialog to set */
    public void setRelationshipTypesForDialog(List<String> relationshipTypesForDialog) {
        this.relationshipTypesForDialog = relationshipTypesForDialog;
    }

    /** @return the addEditDialogRelationships */
    public List<SlotRelationshipView> getAddEditDialogRelationships() {
        return addEditDialogRelationships;
    }

    /**@return the contains tree */
    public SlotRelationshipTree getContainsTree() {
        return containsTree;
    }

    /** @return is existing relationship being edited */
    public boolean isEditExistingRelationship() {
        return editExistingRelationship;
    }

    /** @return selection mode of the selection tree (either "single" or "multiple"). */
    public String getRelationshipTreeSelectionMode() {
        return relationshipTreeSelectionMode.toString();
    }

    /** @return relationship name that the user selected in the "Add/Edit" dialog */
    public String getSelectedRelationshipName() {
        return selectedRelationshipName;
    }

    /** @param selectedRelationshipName relationship name that the user selected in the "Add/Edit" dialog */
    public void setSelectedRelationshipName(String selectedRelationshipName) {
        this.selectedRelationshipName = selectedRelationshipName;
    }

    /** @return a list of names of selected source slots. This method is used by the "sourcesTable" table in the
     * "add-relationships.xhtml" dialog for adding/editing relationships. */
    public List<String> getSourceSlotNames() {
        Set<String> sourceSlotNamesWithoutDuplicates = addEditDialogRelationships.stream()
                .map(relationshipView -> relationshipView.getSourceSlot().getName())
                .collect(Collectors.toSet());
        return new ArrayList<>(sourceSlotNamesWithoutDuplicates);
    }

    /** @return a list of names of selected target slots. This method is used by the "targetsTable" table in the
     * "add-relationships.xhtml" dialog for adding/editing relationships. */
    public List<String> getTargetSlotNames() {
        Set<String> targetSlotNamesWithoutDuplicates = addEditDialogRelationships.stream()
                .map(relationshipView -> relationshipView.getTargetSlot().getName())
                .collect(Collectors.toSet());
        return new ArrayList<>(targetSlotNamesWithoutDuplicates);
    }

    /** @return a list of names of filtered source slots. This method is used by the "sourcesTable" table in the
     * "add-relationships.xhtml" dialog for adding/editing relationships. */
    public List<String> getFilteredSourceSlotNames() {
        return filteredSourceRelationshipNames;
    }

    /** @param filteredSourceRelationshipNames a list of filtered source slots to set. */
    public void setFilteredSourceSlotNames(List<String> filteredSourceRelationshipNames) {
        this.filteredSourceRelationshipNames = filteredSourceRelationshipNames;
    }

    /** @return a list of names of filtered target slots. This method is used by the "targetsTable" table in the
     * "add-relationships.xhtml" dialog for adding/editing relationships. */
    public List<String> getFilteredTargetSlotNames() {
        return filteredTargetRelationshipNames;
    }

    /** @param filteredTargetRelationshipNames a list of filtered target slots to set. */
    public void setFilteredTargetSlotNames(List<String> filteredTargetRelationshipNames) {
        this.filteredTargetRelationshipNames = filteredTargetRelationshipNames;
    }
}
