/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.views;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.io.FilenameUtils;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.BinaryData;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.GenericArtifact;
import org.openepics.discs.conf.ent.NamedEntity;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ui.util.Summary;
import org.openepics.discs.conf.util.UnhandledCaseException;
import org.primefaces.event.FileUploadEvent;

import com.google.common.io.ByteStreams;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 *
 * @param <E> the type of the view parent entity
 */
public class EntityAttrArtifactView<E extends ConfigurationEntity & NamedEntity> extends EntityAttributeView<E> {
    private static final long serialVersionUID = 1L;

    private final Artifact entity;
    private boolean fileRejected = false;
    private EntityWithArtifacts parent;

    /**
     * @param entity the {@link Artifact}
     * @param viewParent the view parent of the {@link GenericArtifact} (the one selected in the table)
     * @param artifactParent the actual parent of the {@link GenericArtifact} (usually {@link ComponentType})
     * @param <P> the type of the of the actual parent
     */
    public <P extends ConfigurationEntity & NamedEntity>
            EntityAttrArtifactView(Artifact entity, E viewParent, P artifactParent) {
        super(viewParent, artifactParent != null ? artifactParent.getName() : "");
        this.entity = entity;
        setKind(artifactParent == null ? getEntityKind(viewParent) : getEntityKind(artifactParent));
        parent = (EntityWithArtifacts)viewParent;
    }

    /**
     * @param entity the {@link Artifact}
     * @param viewParent the {@link Artifact} parent
     */
    public EntityAttrArtifactView(Artifact entity, E viewParent) {
        this(entity, viewParent, null);
    }

    @Override
    public String getId() {
        return entity.getName();
    }

    @Override
    @NotNull
    @Size(min = 1, max = 128, message = "Name can have at most 128 characters.")
    public String getName() {
        return entity.getName();
    }

    @Override
    public String getValue() {
        return "Download attachment";
    }

    /** @return The user specified {@link GenericArtifact} name. */
    public String getArtifactName() {
        return entity.getName();
    }
    /** Called by the UI input control to set the value.
     * @param artifactName The user specified {@link GenericArtifact} name.
     */
    public void setArtifactName(String artifactName) {
        entity.setName(artifactName);
    }

    /** @return The user specified {@link Artifact} description. */
    @NotNull
    @Size(min = 1, max = 255, message="Description can have at most 255 characters.")
    public String getArtifactDescription() {
        return entity.getDescription();
    }
    /** Called by the UI input control to set the value.
     * @param artifactDescription The user specified {@link Artifact} description.
     */
    public void setArtifactDescription(String artifactDescription) {
        entity.setDescription(artifactDescription);
    }


    /** @return <code>true</code> if a "Modify artifact" dialog is open, <code>false</code> otherwise. */
    public boolean isArtifactBeingModified() {
        return entity.getId() != null;
    }

    @Override
    public Artifact getEntity() {
        return entity;
    }

    /**
     * Uploads file to be saved in the {@link Artifact}
     * @param event the {@link FileUploadEvent}
     */
    public void handleImportFileUpload(FileUploadEvent event) {
        fileRejected = false;
        if(uploadedFileRejected(event)){
            fileRejected = true;
            return;
        }

        try (InputStream inputStream = event.getFile().getInputstream()) {
            byte[] importData = ByteStreams.toByteArray(inputStream);
            this.entity.setName(FilenameUtils.getName(event.getFile().getFileName()));
            this.entity.setBinaryData(new BinaryData(importData));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected boolean uploadedFileRejected(FileUploadEvent event){
        String fileName = FilenameUtils.getName(event.getFile().getFileName());
        boolean artifactIsBeningModified = isArtifactBeingModified();
        for(Artifact existingArtifact : parent.getEntityArtifactList()){
            if(artifactIsBeningModified && existingArtifact.getName().equals(entity.getName())){
                continue;
            }
            if(existingArtifact.getName().equals(fileName)){
                final String formName = getFormId(event.getComponent());
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage(formName + "nameValidationMessage", new FacesMessage(
                        FacesMessage.SEVERITY_ERROR, Summary.ERROR.toString(),
                        "Artifact name must be unique."));
                return true;
            }
        }

        return false;
    }

    public boolean isFileRejected(){
        return fileRejected;
    }

    protected String getFormId(final UIComponent input) {
        UIComponent cmp = input;
        while ((cmp != null) && !(cmp instanceof UIForm)) {
            cmp = cmp.getParent();
        }
        return cmp != null ? cmp.getId() + ":" : "";
    }

    private <P extends ConfigurationEntity> EntityAttributeViewKind getEntityKind(P entity) {
        if (entity instanceof ComponentType)
            return EntityAttributeViewKind.ARTIFACT;
        if (entity instanceof Slot)
            return EntityAttributeViewKind.ARTIFACT;
        if (entity instanceof Device)
            return EntityAttributeViewKind.ARTIFACT;
        throw new UnhandledCaseException();
    }
    
    public String getModifiedBy() {
    	return entity.getModifiedBy();
    }
    
    public Date getModifiedAt() {
    	return entity.getModifiedAt();
    }
}
