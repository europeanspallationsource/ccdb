package org.openepics.discs.conf.ui.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.ViewHandler;
import javax.faces.application.ViewHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class RelativeURLViewHandler extends ViewHandlerWrapper {

    private static final Logger LOGGER = Logger.getLogger(RelativeURLViewHandler.class.getName());
    private ViewHandler baseViewHandler;

    public RelativeURLViewHandler(ViewHandler baseViewHandler) {
        this.baseViewHandler = baseViewHandler;
    }

    @Override
    public String getActionURL(FacesContext context, String viewId) {
        return convertToRelativeURL(context, baseViewHandler.getActionURL(context, viewId));
    }

    @Override
    public String getResourceURL(FacesContext context, String path) {
        return convertToRelativeURL(context, baseViewHandler.getResourceURL(context, path));
    }

    @Override
    public ViewHandler getWrapped() {
        return baseViewHandler;
    }

    private String convertToRelativeURL(FacesContext context, String url) {
        final HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            final URI uri = new URI(request.getRequestURI());
            String path = uri.getPath().replace("//", "/");
            if (url.startsWith("/")) {
                StringBuilder prefix = new StringBuilder(".");
                int count = path.length() - path.replace("/", "").length();
                for (int i = 0; i < (count - 1); i++) {
                    prefix.append("./.");
                }
                return (prefix.toString() + url);
            }
            return url;
        } catch (URISyntaxException e) {
            LOGGER.log(Level.SEVERE, "Malformed URI while converting to relative path", e);
            return url;
        }
    }
}
