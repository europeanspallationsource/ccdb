/**
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.views;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.NamedEntity;
import org.openepics.discs.conf.ent.ExternalLink;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 *
 * @param <E> the type of the view parent entity
 */
public class EntityAttrExternalLinkView<E extends ConfigurationEntity & NamedEntity> extends EntityAttributeView<E> {
    private static final long serialVersionUID = 1L;

    private final ExternalLink externalLink;
    private EntityWithExternalLinks parent;

    /**
     * @param externalLink  the {@link ExternalLink}
     * @param viewParent the view parent of the {@link ExternalLink} (the one selected in the table)
     * @param externalLinksParent the actual parent of the {@link ExternalLink} (usually {@link ComponentType})
     * @param <P> the type of the of the actual parent
     */
    public <P extends ConfigurationEntity & NamedEntity>
            EntityAttrExternalLinkView(ExternalLink externalLink, E viewParent, P externalLinksParent) {
        super(viewParent, externalLinksParent != null ? externalLinksParent.getName() : "");
        this.externalLink = externalLink;
        setKind(EntityAttributeViewKind.EXTERNAL_LINK);
        parent = (EntityWithExternalLinks)viewParent;
    }

    /**
     * @param externalLink  the {@link ExternalLink}
     * @param viewParent the {@link ExternalLink} parent
     */
    public EntityAttrExternalLinkView(ExternalLink externalLink, E viewParent) {
        this(externalLink, viewParent, null);
    }


    @Override
    public String getId() {
        return externalLink.getName();
    }

    @Override
    @NotNull
    @Size(min = 1, max = 128, message = "Name can have at most 128 characters.")
    public String getName() {
        return externalLink.getName();
    }

    public void setName(String name){
        externalLink.setName(name);
    }

    @Override
    public String getValue() {
        return getUri();
    }

    public void setUri(String uri){
        externalLink.setUri(uri);
    }

    public String getUri(){
        return externalLink.getUri();
    }

    /** @return The user specified {@link Artifact} description. */
    @NotNull
    @Size(min = 1, max = 255, message="Description can have at most 255 characters.")
    public String getDescription() {
        return externalLink.getDescription();
    }
    /** Called by the UI input control to set the value.
     * @param description  The user specified {@link ExternalLink} description.
     */
    public void setDescription(String description) {
        externalLink.setDescription(description);
    }

    /** @return <code>true</code> if a "Modify artifact" dialog is open, <code>false</code> otherwise. */
    public boolean isExternalLinkBeingModified() {
        return externalLink.getId() != null;
    }

    @Override
    public ExternalLink getEntity() {
        return externalLink;
    }
}
