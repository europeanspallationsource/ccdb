/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.discs.conf.ui;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.openepics.discs.conf.util.AppProperties;

/**
 * Manager for loading footer properties.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Named
@ViewScoped
public class FooterManager implements Serializable {
    
	private static final long serialVersionUID = -6002902957562442098L;

	private static final Logger LOGGER = Logger.getLogger(FooterManager.class.getCanonicalName());
	
    @Inject
    private AppProperties properties;
	
    /** @return the support email */
    public String getSupportEmail() {
    	final String supportEmail = properties.getProperty(AppProperties.SUPPORT_EMAIL); 
        LOGGER.log(Level.FINE, "Support email: " + supportEmail);
        return supportEmail;
    }
}
