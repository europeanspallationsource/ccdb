/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.SlotValidator;
import org.openepics.discs.conf.util.names.Names;

/**
 * This handles refresh invocations from ui. 
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@SessionScoped
public class RefreshManager implements Serializable {
	private static final long serialVersionUID = 1041229738711370019L;

    private static final Logger LOGGER = Logger.getLogger(RefreshManager.class.getName());
	
    @Inject private Names names;
    @Inject private SlotValidator slotValidator;
    
    /**
     * Refreshes all caches and database validity.
     */
    public void refresh() {
    	LOGGER.fine("Refresh started");
        names.refresh();
        UiUtility.showMessage(FacesMessage.SEVERITY_INFO, "Naming cache refreshed.", null);
        UiUtility.updateComponent("menumsgs");
        slotValidator.validate();
        UiUtility.showMessage(FacesMessage.SEVERITY_INFO, "Validation of cables finished.", null);
    	LOGGER.fine("Refresh completed");
    }
}
