/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.auditlog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.EntityTypeOperation;
import org.openepics.discs.conf.ent.GenericArtifact;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;

/**
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 *
 */
public class InstallationRecordEntityLoggerTest {
    private InstallationRecord installationRecord;
    private final Slot slot = new Slot("slot1", true);
    private final Device device = new Device("device1");
    private final GenericArtifact artifact1 = new GenericArtifact();
    private final GenericArtifact artifact2 = new GenericArtifact();

    private final InstallationRecordEntityLogger installationRecordEntityLogger = new InstallationRecordEntityLogger();

    private static final String TEST_DATE_STRING = "1970-01-03 12:12:03";

    private static Date TEST_DATE;
    static {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			TEST_DATE = dateFormatGmt.parse(TEST_DATE_STRING);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		} 
    }
    
    @Before
    public void setUp() {
        artifact1.setName("CAT Image");
        artifact1.setDescription("Simple CAT image");
        artifact2.setName("Manual");
        artifact2.setDescription("Users manual");
        installationRecord = new InstallationRecord("record1", TEST_DATE);
        slot.setComponentType(new ComponentType());
        installationRecord.setSlot(slot);
        installationRecord.setDevice(device);
        installationRecord.getArtifactList().add(artifact1);
        installationRecord.getArtifactList().add(artifact2);
        device.getInstallationRecordList().add(installationRecord);
        slot.getInstallationRecordList().add(installationRecord);

    }

    @Test
    public void testGetType() {
        assertTrue(InstallationRecord.class.equals(installationRecordEntityLogger.getType()));
    }

    @Test
    public void testSerializeEntityInstallationDate() {
        final String DEVICE_LOG_ENTRY = "{\"name\":\"device1\",\"installation\":[{\"installationDate\":\"" + TEST_DATE_STRING +"\"},"
                + "{\"name\":\"slot1\"}]}";
        assertEquals(DEVICE_LOG_ENTRY, installationRecordEntityLogger.auditEntries(installationRecord,
                                                                        EntityTypeOperation.CREATE).get(0).getEntry());

        final String SLOT_LOG_ENTRY = "{\"name\":\"slot1\",\"hostingSlot\":true,\"installation\":[{\"installationDate\":"
                + "\"" + TEST_DATE_STRING +"\"},{\"name\":\"device1\"}]}";
        assertEquals(SLOT_LOG_ENTRY, installationRecordEntityLogger.auditEntries(installationRecord,
                                                                        EntityTypeOperation.CREATE).get(1).getEntry());
    }

    @Test
    public void testSerializeEntityUninstallDate() {
        installationRecord.setUninstallDate(TEST_DATE);
        final String DEVICE_LOG_ENTRY = "{\"name\":\"device1\",\"installation\":[{\"installationDate\":\"" + TEST_DATE_STRING +"\"},"
                + "{\"name\":\"slot1\"},{\"uninstallationDate\":\"" + TEST_DATE_STRING +"\"}]}";
        assertEquals(DEVICE_LOG_ENTRY, installationRecordEntityLogger.auditEntries(installationRecord,
                                                                        EntityTypeOperation.CREATE).get(0).getEntry());

        final String SLOT_LOG_ENTRY = "{\"name\":\"slot1\",\"hostingSlot\":true,\"installation\":[{\"installationDate\":"
                + "\"" + TEST_DATE_STRING +"\"},{\"name\":\"device1\"},{\"uninstallationDate\":\"" + TEST_DATE_STRING +"\"}]}";
        assertEquals(SLOT_LOG_ENTRY, installationRecordEntityLogger.auditEntries(installationRecord,
                                                                        EntityTypeOperation.CREATE).get(1).getEntry());
    }
}
