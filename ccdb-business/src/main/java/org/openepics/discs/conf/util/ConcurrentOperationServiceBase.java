/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * This is base for timed services functionality that supports handling timed operations like validation and cache
 * update from concurrent requests.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Lock(LockType.READ)
@TransactionAttribute(TransactionAttributeType.NEVER)
public abstract class ConcurrentOperationServiceBase {
    private static final Logger LOGGER = Logger.getLogger(ConcurrentOperationServiceBase.class.getName());

    protected boolean initialized;
    private boolean valid;

    private ReentrantLock lock = new ReentrantLock();

    /**
     * Extend this function to provide service name used for logging.
     * 
     * @return the service name.
     */
    protected abstract String getServiceName();

    /**
     * Extend this function to provide operation used for logging.
     * 
     * @return the operation.
     */
    protected abstract String getOperation();

    /**
     * Extend this function to run timed operation.
     * 
     * @param vargs
     *            arguments to pass on to operation
     * 
     * @return true if the operation was successfully run, else false.
     */
    protected abstract boolean runOperation(String... vargs);

    protected void initialise() {
        LOGGER.log(Level.INFO, getServiceName() + "  is initializing.");
        runOperationWithLock();
    }

    /**
     * Runs timed operation and returns when finished. If operation is already in progress when the method is invoked,
     * it returns when that operation completes.
     * 
     * @param vargs
     *            parameters to pass on to opertaion
     */
    public void runOperationBlocking(String... vargs) {
        boolean completed = runOperationWithLock(vargs);
        if (completed) {
            return;
        }

        LOGGER.log(Level.INFO,
                getServiceName() + ": " + getOperation() + " is already running. Will wait until it is done.");
        // We try to acquire lock waiting for 20 seconds. If unsuccessful we try until successful.
        try {
            while (!lock.tryLock(20, TimeUnit.SECONDS))
                ;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        lock.unlock();
        LOGGER.log(Level.INFO, getServiceName() + ": " + getOperation() + " finished.");
    }

    /**
     * Runs operation and returns when finished. If operation is already in progress when the method is invoked, it
     * returns immediately.
     * 
     * @param vargs
     *            parameters to pass on to opertaion
     */
    public void runOperationNonBlocking(String... vargs) {
        boolean completed = runOperationWithLock(vargs);
        if (!completed) {
            LOGGER.log(Level.INFO, getServiceName() + ": " + getOperation() + " is alreday in progress. Will skip.");
        }
    }

    /**
     * Runs operation and returns finished. If operation is already in progress when the method is invoked, it returns
     * immediately and returns false.
     * 
     * @return true if the operation was run, else false
     */
    private boolean runOperationWithLock(String... vargs) {
        if (lock.tryLock()) {
            try {
                LOGGER.log(Level.INFO, getServiceName() + ": Running " + getOperation() + ".");
                valid = runOperation(vargs);
                initialized = true;
                LOGGER.log(Level.INFO, getServiceName() + ": " + getOperation() + " finished.");
            } finally {
                lock.unlock();
            }
            return true;
        } else {
            return false;
        }
    }

    /** @return true if the cache contains valid data, else false */
    public boolean isValid() {
        return valid;
    }
}
