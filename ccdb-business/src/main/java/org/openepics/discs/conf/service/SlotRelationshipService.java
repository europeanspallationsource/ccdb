/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.service;

import com.google.common.base.Preconditions;
import org.openepics.discs.conf.dl.common.ErrorMessage;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotRelation;
import org.openepics.discs.conf.ent.SlotRelationName;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This is a service that handles slot relationship operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class SlotRelationshipService {
    
    private static final String RELATIONSHIP_SELF_ERROR = "The slot cannot be in relationship with itself.";
    private static final String RELATIONSHIP_EXISTS_ERROR = "This relationship already exists.";
    private static final String SLOT_PAIR_LOOP_ERROR = "Relationship 'contains' between these two slots cannot be " + 
            "created because this would result in loop.";

    @Inject private SlotEJB slotEJB;
    @Inject private SlotPairEJB slotPairEJB;

    /**
     * Creates slot relationships for all the combinations of source slots and target slots, stores them into 
     * database and returns them.
     * 
     * @param sourceSlots a list of source slots.
     * @param targetSlots a list of target slots.
     * @param newSlotRelation the name of relationship (contains, controls, ...).
     * @param isForwardRelation a relationship between source-target and parent-child directions. If true, 
     *                          source is parent and target is child. If false, source is child and target is parent.  
     * @return a list of slot relationships created.
     */
    public List<SlotRelationship> createRelationships(List<Slot> sourceSlots, List<Slot> targetSlots, 
            SlotRelation newSlotRelation, boolean isForwardRelation) throws SlotRelationshipServiceException {
        Preconditions.checkNotNull(sourceSlots);
        Preconditions.checkNotNull(targetSlots);
        Preconditions.checkNotNull(newSlotRelation);
        Preconditions.checkArgument(sourceSlots.size() == 1 || targetSlots.size() == 1);
        
        final List<SlotRelationship> addedRelationships = new ArrayList<>();
        for (final Slot sourceSlot : sourceSlots) {
            for (final Slot targetSlot : targetSlots) {
                final Slot parentSlot;
                final Slot childSlot;
                if (isForwardRelation) {
                    childSlot = targetSlot;
                    parentSlot = sourceSlot;
                } else {
                    childSlot = sourceSlot;
                    parentSlot = targetSlot;
                }

                checkCreateUpdateRelationship(parentSlot, childSlot, newSlotRelation);
                
                final SlotPair newSlotPair = new SlotPair();
                newSlotPair.setChildSlot(childSlot);
                newSlotPair.setParentSlot(parentSlot);
                newSlotPair.setSlotRelation(newSlotRelation);

                if (slotPairEJB.slotPairCreatesLoop(newSlotPair, childSlot)) {
                    throw new SlotRelationshipServiceException(SLOT_PAIR_LOOP_ERROR);
                }
                
                slotPairEJB.add(newSlotPair);
                
                SlotRelationship addedRelationship = new SlotRelationship();
                addedRelationship.setSourceSlot(sourceSlot);
                addedRelationship.setTargetSlot(targetSlot);
                addedRelationship.setRelationshipName(newSlotRelation.getNameAsString());
                addedRelationship.setSlotPair(newSlotPair);
                addedRelationships.add(addedRelationship);
            }
        }
        return addedRelationships;
    }

    /* Performs various relationship checks, prints an error message. Throws exception if the the checks fail,
     * returns silently if the create/update operation can proceed.
     */
    private void checkCreateUpdateRelationship(final Slot parentSlot, final Slot childSlot,
            final SlotRelation newSlotRelation) throws SlotRelationshipServiceException {
        if (childSlot.equals(parentSlot)) {
            throw new SlotRelationshipServiceException(RELATIONSHIP_SELF_ERROR);
        } else if (!slotPairEJB.findSlotPairsByParentChildRelation(childSlot.getName(), parentSlot.getName(),
                newSlotRelation.getName()).isEmpty()) {
            throw new SlotRelationshipServiceException(RELATIONSHIP_EXISTS_ERROR);
        }
    }

    /**
     * Updates existing slot relationships in the database and returns them.
     * 
     * @param existingRelationships a list of slot relationships to update.
     * @param newTargetSlot the new target slot.
     * @param newSlotRelation the new name of relationship (contains, controls, ...).
     * @return a list of slot relationships updated.
     */
    public List<SlotRelationship> updateRelationships(List<SlotRelationship> existingRelationships, 
            Slot newTargetSlot, SlotRelation newSlotRelation) throws SlotRelationshipServiceException {
        Preconditions.checkNotNull(existingRelationships);
        Preconditions.checkArgument(!existingRelationships.isEmpty());
        Preconditions.checkNotNull(newTargetSlot);
        Preconditions.checkNotNull(newSlotRelation);
        
        final Set<Long> updatedSlots = new HashSet<>();
        final List<SlotRelationship> updatedRelationships = new ArrayList<>();
        for (final SlotRelationship existingRelationship : existingRelationships) {
            final Slot parentSlot;
            final Slot childSlot;
            // if the user selects a new target slot the previous target is considered "lost"
            final Slot lostSlot;
            final boolean relationDirectionUnchanged =
                    newSlotRelation.getNameAsString().equals(existingRelationship.getRelationshipName());
            if (relationDirectionUnchanged) {
                childSlot = newTargetSlot;
                parentSlot = existingRelationship.getSourceSlot();
                lostSlot = getLostSlot(existingRelationship.getSlotPair(), parentSlot, childSlot);
            } else {
                childSlot = existingRelationship.getSourceSlot();
                parentSlot = newTargetSlot;
                lostSlot = getLostSlot(existingRelationship.getSlotPair(), childSlot, parentSlot);
            }

            final SlotPair editedSlotPair = existingRelationship.getSlotPair();
            if (editedSlotPair.getChildSlot().equals(childSlot) &&
                    editedSlotPair.getParentSlot().equals(parentSlot) &&
                    editedSlotPair.getSlotRelation().equals(newSlotRelation)) {
                // nothing to do, relationship not modified. Skip the current slot.
                continue;
            }
            
            checkCreateUpdateRelationship(parentSlot, childSlot, newSlotRelation);
            checkChildSlotForOrphan(existingRelationship.getRelationshipName(), 
                    existingRelationship.getSlotPair());

            // before creating a new relationship, we need to find the original one in the parent and the child
            final int parentRelationshipIndex = 
                    parentSlot.getPairsInWhichThisSlotIsAParentList().indexOf(editedSlotPair);
            final int childRelationshipIndex = childSlot.getPairsInWhichThisSlotIsAChildList().indexOf(editedSlotPair);

            if (lostSlot != null) {
                // update the "lost" slot. We don't know in which list the relationship was, so we try both.
                lostSlot.getPairsInWhichThisSlotIsAChildList().remove(editedSlotPair);
                lostSlot.getPairsInWhichThisSlotIsAParentList().remove(editedSlotPair);
            }

            // change the relationship
            editedSlotPair.setChildSlot(childSlot);
            editedSlotPair.setParentSlot(parentSlot);
            editedSlotPair.setSlotRelation(newSlotRelation);
            if (!relationDirectionUnchanged) {
                editedSlotPair.setSlotOrder(slotPairEJB.getHighestOrderNumberForNewPair(parentSlot));
            }

            // update the relationship parent and child slots for logging
            if (parentRelationshipIndex > -1) {
                parentSlot.getPairsInWhichThisSlotIsAParentList().set(parentRelationshipIndex, editedSlotPair);
            } else {
                parentSlot.getPairsInWhichThisSlotIsAParentList().add(editedSlotPair);
            }

            if (childRelationshipIndex > -1) {
                childSlot.getPairsInWhichThisSlotIsAChildList().set(childRelationshipIndex, editedSlotPair);
            } else {
                childSlot.getPairsInWhichThisSlotIsAChildList().add(editedSlotPair);
            }

            // check the new relationship before committing everything
            if (slotPairEJB.slotPairCreatesLoop(editedSlotPair, childSlot)) {
                throw new SlotRelationshipServiceException(SLOT_PAIR_LOOP_ERROR);
            }

            slotPairEJB.save(editedSlotPair);

            // save the lost slot for logging
            if (lostSlot != null && !updatedSlots.contains(lostSlot.getId())) {
                slotEJB.save(lostSlot);
                updatedSlots.add(lostSlot.getId());
            }
            
            SlotRelationship updatedRelationship = new SlotRelationship();
            updatedRelationship.setId(existingRelationship.getId());
            updatedRelationship.setRelationshipName(newSlotRelation.getNameAsString());
            updatedRelationship.setSourceSlot(existingRelationship.getSourceSlot());
            updatedRelationship.setTargetSlot(newTargetSlot);
            updatedRelationship.setSlotPair(slotPairEJB.refreshEntity(editedSlotPair));
            updatedRelationships.add(updatedRelationship);
        }
        return updatedRelationships;
    }

    /* This method returns the Slot that will be replaced by a new target, in case the user selected a new target.
     * If the user did not select a new target Slot but just changed the relationship type, this method return null.
     * The "source" is the Slot that cannot change.
     */
    private Slot getLostSlot(final SlotPair originalPair, final Slot source, final Slot newTarget) {
        final Long targetId = newTarget.getId();
        final Long sourceId = source.getId();
        if (sourceId.equals(originalPair.getParentSlot().getId())) {
            // source is parent
            if (targetId.equals(originalPair.getChildSlot().getId())) {
                // target did not change
                return null;
            } else {
                return originalPair.getChildSlot();
            }
        } else {
            // source is child
            if (targetId.equals(originalPair.getParentSlot().getId())) {
                // target did not change
                return null;
            } else {
                return originalPair.getParentSlot();
            }
        }
    }
    
    private void checkChildSlotForOrphan(final String relationshipName, final SlotPair slotPair) 
            throws SlotRelationshipServiceException {
        if ((relationshipName.equals(SlotRelationName.CONTAINS.toString()) )|| 
                relationshipName.equals(SlotRelationName.CONTAINS.inverseName()) && 
                !slotPairEJB.slotHasMoreThanOneContainsRelation(slotPair.getChildSlot())) {
            String errorMessage = String.format("%s: parent slot: %s, child slot: %s, relationship name: %s",
                    ErrorMessage.ORPHAN_CREATED_BY_EDIT.toString(),
                    slotPair.getParentSlot().getName(),
                    slotPair.getChildSlot().getName(),
                    relationshipName);
            throw new SlotRelationshipServiceException(errorMessage);
        }
    }

    /**
     * Deletes existing slot relationships from the database and returns them.
     * 
     * @param existingRelationships a list of slot relationships to delete.
     * @return a list of deleted slot relationships.
     */
    public List<SlotRelationship> deleteRelationships(List<SlotRelationship> existingRelationships) {
        Preconditions.checkNotNull(existingRelationships);
        Preconditions.checkArgument(!existingRelationships.isEmpty());
        
        List<SlotPair> existingSlotPairs = existingRelationships.stream()
                .map(relationship -> relationship.getSlotPair())
                .collect(Collectors.toList());
        for (SlotPair existingSlotPair : existingSlotPairs) {
            // update contained slots, because removal needs to be logged
            existingSlotPair.getParentSlot().getPairsInWhichThisSlotIsAParentList().remove(existingSlotPair);
            existingSlotPair.getChildSlot().getPairsInWhichThisSlotIsAChildList().remove(existingSlotPair);
            slotPairEJB.delete(existingSlotPair);
        }
        return existingRelationships;
    }
}
