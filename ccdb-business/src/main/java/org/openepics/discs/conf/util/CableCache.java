package org.openepics.discs.conf.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.openepics.cable.client.CableDBClient;
import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.CableResource;
import org.openepics.discs.conf.util.AppProperties;

import com.google.common.base.Preconditions;

/**
 * This handles loading cable information from cable database through {@link CableDBClient} and caching
 * the data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
public class CableCache extends ConcurrentOperationServiceBase implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(CableCache.class.getName());
    @Inject private AppProperties properties;

	/** The interval for refreshing cache: 5 minutes */
    private static final String UPDATE_INTERVAL = "*/5";

    private boolean cableDBStatus = false;
    private List<CableElement> cables = new ArrayList<>();

	@PostConstruct
    protected void initialise() {
    	super.initialise();
    }

    /** Performs periodic update of the cable cache. */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        runOperationNonBlocking();
    }

	@Override
	protected String getServiceName() {
		return "Cable Database";
	}

    @Override
    protected String getOperation() {
        return "Cache update";
    }

    /**
     * Update cache (cables). Note <tt>@Lock(LockType.WRITE)</tt> for update of shared resources.
     */
    @Lock(LockType.WRITE)
	@Override
    protected boolean runOperation(String... vargs) {
        Preconditions.checkArgument(vargs.length == 0);
        cableDBStatus = properties.getBooleanPropety(AppProperties.CABLEDB_STATUS);
        if (cableDBStatus) {
            try {
                LOGGER.log(Level.INFO, "Obtaining the cable information.");
                CableResource cr = (new CableDBClient(null)).createCableResource();
                List<CableElement> allCables = cr.getAllCables(Collections.emptyList(), "");
                if (allCables != null) {
                    cables = allCables;
                } else {
                    cables = Arrays.asList();
                }
                LOGGER.log(Level.FINER, "Got " + cables.size() + " cables.");
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Error obtaining the CABLE information.", e);
        		return false;
            }
        }
        return true;
	}

    /**
     * Returns a list of all cables currently present in cable database.
     *
     * @return the list
     */
    public List<CableElement> getAllCables() {
    	if (!initialized) {
    		runOperationBlocking();
    	}
    	return cables;
    }
    /** @return the cableDBStatus */
    public boolean getCableDBStatus() {
        return cableDBStatus;
    }
}
