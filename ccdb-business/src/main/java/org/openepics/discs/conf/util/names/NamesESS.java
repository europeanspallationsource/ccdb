package org.openepics.discs.conf.util.names;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.interceptor.Interceptor;

import org.openepics.discs.conf.util.AppProperties;
import org.openepics.discs.conf.util.ConcurrentOperationServiceBase;
import org.openepics.names.jaxb.DeviceNameElement;

import com.google.common.base.Preconditions;

/**
 * A session bean holding caching the data retrieved from {@link NamesService}. This class ensures that the possibly
 * slow read operations from underlying services are not performed more than once in a given session.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
@Alternative
@Priority(Interceptor.Priority.APPLICATION+10)
public class NamesESS extends ConcurrentOperationServiceBase implements Names, Serializable {
    private static final Logger LOGGER = Logger.getLogger(NamesESS.class.getName());
    private static final long serialVersionUID = 1L;

    // the time to wait before updating name service cache (20 min )
    private static final String UPDATE_INTERVAL = "*/20";

    @Inject
    private transient NamesService namesService;
    @Inject
    private AppProperties properties;

    private static boolean valid;

    private Map<String, NameStatus> namesStatus;
    private Map<String, String> uuidNames;
    private Map<String, String> namesUuid;
    private Map<String, DeviceNameElement> devices;

    @PostConstruct
    protected void initialise() {
        super.initialise();
    }

    /**
     * Updated name service cache.
     *
     */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        runOperationNonBlocking();
    }

    @Override
    protected String getServiceName() {
        return "Names Service Cache";
    }

    @Override
    protected String getOperation() {
        return "Cache update";
    }

    /**
     * 	Update cache (names, devices). Note <tt>@Lock(LockType.WRITE)</tt> for update of shared resources.
     */
    @Lock(LockType.WRITE)
    @Override
    protected boolean runOperation(String... vargs) {
        Preconditions.checkArgument(vargs.length == 0);

        Map<String, DeviceNameElement> newDevices = updateDevices();
        Map<String, NameStatus> newNamesStatus = generateNamesStatusMap(newDevices);
        Map<String, String> newUuidNames = generateUuidNamesMap(newDevices, newNamesStatus);
        Map<String, String> newNamesUuid = generateNamesUuidMap(newDevices);

        devices = newDevices;
        namesStatus = newNamesStatus;
        uuidNames = newUuidNames;
        namesUuid = newNamesUuid;
        return true;
    }

    //	to be private since method calling this method is locktype.write
    private Map<String, DeviceNameElement> updateDevices() {
        Map<String, DeviceNameElement> newDevices = Collections.emptyMap();
        if (isEnabled()) {
            try {
                newDevices = namesService
                        .getAllDevices()
                        .stream()
                        .filter(element -> element != null)
                        .collect(
                                Collectors.toMap(DeviceNameElement::getName,
                                        Function.identity()));
                valid = true;
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "There was an exception retrieving devices from the naming service.", e);
                valid = false;
            }
        }
        return newDevices;
    }

    @Override
    public boolean isEnabled() {
        return properties.getBooleanPropety(AppProperties.NAMING_DETECT_STATUS);
    }

    @Override
    public boolean isError() {
        return !valid;
    }

    @Override
    public void refresh() {
        runOperationBlocking();
    }

    @Override
    public Map<String, DeviceNameElement> getAllNames() {
        if (!initialized) {
            runOperationBlocking();
        }
        return devices;
    }

    private Map<String, NameStatus> generateNamesStatusMap(Map<String, DeviceNameElement> newDevices) {
        Map<String, NameStatus> namesStatus = new HashMap<String, NameStatus>();
        for (DeviceNameElement element : newDevices.values()) {
            NameStatus deviceStatus = NameStatus.NONE;
            try {
                deviceStatus = NameStatus.valueOf(element.getStatus());
            } catch (NullPointerException | IllegalArgumentException e) {
                LOGGER.log(
                        Level.WARNING,
                        String.format("Unknown device status received: %s, setting to None", element.getStatus()),
                        e);
            }
            namesStatus.put(element.getName(), deviceStatus);
        }
        return namesStatus;
    }

    private Map<String, String> generateUuidNamesMap(Map<String, DeviceNameElement> newDevices,
            Map<String, NameStatus> newNamesStatus) {
        Map<String, String> uuidNames = new HashMap<String, String>();
        for (DeviceNameElement element : newDevices.values()) {
            NameStatus deviceStatus = newNamesStatus.get(element.getName());
            if (deviceStatus != null && deviceStatus != NameStatus.OBSOLETE) {
                uuidNames.put(element.getUuid().toString(), element.getName());
            }
        }
        return uuidNames;
    }

    private Map<String, String> generateNamesUuidMap(Map<String, DeviceNameElement> newDevices) {
        Map<String, String> namesUuid = new HashMap<String, String>();
        for (DeviceNameElement element : newDevices.values()) {
            namesUuid.put(element.getName(), element.getUuid().toString());
        }
        return namesUuid;
    }

    @Override
    public Set<String> getActiveNames() {
        if (!initialized) {
            runOperationBlocking();
        }
        Set<String> activeNames = new HashSet<String>();
        // more efficient to use iterator on entrySet of map than key retrieved from keySet iterator
        // to avoid map.get(key) lookup
        for (Map.Entry<String, NameStatus> nameStatusEntry : namesStatus.entrySet()) {
            if (nameStatusEntry.getValue() == NameStatus.ACTIVE) {
                activeNames.add(nameStatusEntry.getKey());
            }
        }
        return activeNames;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public NameStatus getNameStatus(String deviceName) {
        if (!initialized) {
            runOperationBlocking();
        }
        NameStatus status = namesStatus.get(deviceName);
        return (status == null) ? NameStatus.NONE : status;
    }

    @Override
    public String getNameByUuid(String uuid) {
        if (!initialized) {
            runOperationBlocking();
        }
        return uuidNames.get(uuid);
    }

    @Override
    public String getUuidByName(String name) {
        if (!initialized) {
            runOperationBlocking();
        }
        return namesUuid.get(name);
    }
}
