/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.util.names.Names;

/**
 * This is a service that performs periodic validation of the slots.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class SlotValidationService {

    private static final Logger LOGGER = Logger.getLogger(SlotValidationService.class.getName());
	
    @Inject
    private SlotEJB slotEJB;
	
    @Inject
    private Names names;

    /**
     * Validates and updates information on a group of slots.
     *  
     * @param first the index of the first slot
     * @param size the number of slots to validate after first
     * @param username the username used for validation
     * 
     * @return true if validation was performed on any slots, false otherwise
     */
    public boolean validateSlots(int first, int size, String username) {
    	List<Slot> slots = slotEJB.findAllSlots(first, size);
        validateSlots(slots, true, username);
    	return !slots.isEmpty();
    }
    
    /**
     * Validates and updates information on the slot. 
     * @param slot the slot to validate
     */
    public void validateSlot(Slot slot) {
    	validateSlots(Arrays.asList(slot), false, null);
    }
    
    /**
     * Validates and updates information on a slot. 
     * @param slots the slots to validate
     * @param save if true the changes to slot are persisted
     * @param username the username used for validation
     */
    public void validateSlots(List<Slot> slots, boolean save, String username) {
    	List<Slot> changedSlots = new ArrayList<Slot>(); 
    	for (final Slot slot: slots) {
	    	if (validateSlotInternal(slot)) {
	    		changedSlots.add(slot);
	    	}
    	}
    	if (save) {
    		for (final Slot slot: changedSlots) {
        		slotEJB.saveInternal(slot, username);
        	}
    	}
    }
    
    /**
     * Validates and updates information on the slot. 
     * @param slot the slot to validate
     * 
     * @return whether slot information has changed
     */
    private boolean validateSlotInternal(Slot slot) {
    	boolean changed = false;
    	if (names.isEnabled() && slot.isHostingSlot()) {
	    	if (slot.getNameUuid() == null) {
	    		final String uuid = names.getUuidByName(slot.getName()); 
	    		if (uuid != null && !uuid.equals(slot.getNameUuid())) {
	    			LOGGER.fine(String.format("Updating %s: uuid: %s -> %s", slot.getName(), slot.getNameUuid(), uuid));
	    			slot.setNameUuid(uuid);
	    	    	changed = true;
	    		}
	    	} else {
	    		String name = names.getNameByUuid(slot.getNameUuid()); 
	    		if (name != null && !name.equals(slot.getName())) {
	    			LOGGER.fine(String.format("Updating %s -> %s", slot.getName(), name));
	    			slot.setName(name);
	    	    	changed = true;
	    		}
	    	}
    	}
    	return changed;
    }
}
