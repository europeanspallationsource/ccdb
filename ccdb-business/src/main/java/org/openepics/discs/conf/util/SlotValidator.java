/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

/**
 * This is a service that performs periodic validation of slots.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
@TransactionAttribute(TransactionAttributeType.NEVER)
public class SlotValidator extends ConcurrentOperationServiceBase {

    private static final Logger LOGGER = Logger.getLogger(SlotValidator.class.getName());
    private static final int VALIDATION_BUNCH = 1000;
    // the time to wait between individual validations (20 min )
    private static final String VALIDATION_INTERVAL = "*/20";

    @Resource
    private SessionContext ctx;
    @Inject
    private SlotValidationService slotValidationService;

    /**
     * Performs periodic cable validation.
     *
     */
    @Schedule(minute = VALIDATION_INTERVAL, hour = "*", persistent = false)
    public void validate() {
        runOperationNonBlocking();
    }

    @Override
    protected String getServiceName() {
        return "SlotValidator";
    }

    @Override
    protected String getOperation() {
        return "Validation";
    }

    /**
     * Validation of slots. Note <tt>@Lock(LockType.WRITE)</tt> for update of shared resources.
     */
    @Lock(LockType.WRITE)
    @Override
    protected boolean runOperation(String... vargs) {
        int bunch = 0;
        while (slotValidationService.validateSlots(VALIDATION_BUNCH * bunch, VALIDATION_BUNCH, getServiceName())) {
            LOGGER.log(Level.INFO, String.format("Validation of slot bunch %s completed.", bunch));
        	bunch++;
        }
        return true;
    }
}
