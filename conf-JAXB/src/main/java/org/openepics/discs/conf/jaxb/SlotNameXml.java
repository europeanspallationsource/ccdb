/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.jaxb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This is data transfer object representing a CCDB slot for JSON and XML serialization. This object only carries
 * basic information, like slot name.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@XmlRootElement(name = "name")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ SlotReferenceXml.class})
public class SlotNameXml {
    @XmlElement
    protected Long id;
    @XmlElement
    protected String nameId;
	@XmlElement
    protected String name;

	@XmlElement(name = "type")
    protected SlotTypeXml slotType;

    @XmlElementWrapper(name = "parents")
    @XmlAnyElement(lax = true)
    protected List<SlotReferenceXml> parents = new ArrayList<>();

    @XmlElementWrapper(name = "children")
    @XmlAnyElement(lax = true)
    protected List<SlotReferenceXml> children = new ArrayList<>();

    @XmlElementWrapper(name = "powers")
    @XmlAnyElement(lax = true)
    protected List<SlotReferenceXml> powers = new ArrayList<>();

    @XmlElementWrapper(name = "poweredBy")
    @XmlAnyElement(lax = true)
    protected List<SlotReferenceXml> poweredBy = new ArrayList<>();

    @XmlElementWrapper(name = "controls")
    @XmlAnyElement(lax = true)
    protected List<SlotReferenceXml> controls = new ArrayList<>();

    @XmlElementWrapper(name = "controlledBy")
    @XmlAnyElement(lax = true)
    protected List<SlotReferenceXml> controlledBy = new ArrayList<>();
    
    public SlotNameXml() {}

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNameId() { return nameId; }
    public void setNameId(String nameId) { this.nameId = nameId; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public SlotTypeXml getSlotType() { return slotType; }
    public void setSlotType(SlotTypeXml slotType) { this.slotType = slotType; }

    public List<SlotReferenceXml> getParents() { return parents; }
    public void setParents(List<SlotReferenceXml> parents) { this.parents = parents; }

    public List<SlotReferenceXml> getChildren() { return children; }
    public void setChildren(List<SlotReferenceXml> children) { this.children = children; }

    public List<SlotReferenceXml> getPowers() { return powers; }
    public void setPowers(List<SlotReferenceXml> powers) { this.powers = powers; }

    public List<SlotReferenceXml> getPoweredBy() { return poweredBy; }
    public void setPoweredBy(List<SlotReferenceXml> poweredBy) { this.poweredBy = poweredBy; }

    public List<SlotReferenceXml> getControls() { return controls; }
    public void setControls(List<SlotReferenceXml> controls) { this.controls = controls; }

    public List<SlotReferenceXml> getControlledBy() { return controlledBy; }
    public void setControlledBy(List<SlotReferenceXml> controlledBy) { this.controlledBy = controlledBy; }
    
    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("SlotNameXml[");
        str.append("id=").append(id);
        str.append(", nameId=").append(nameId);
        str.append(", name=").append(name);
        str.append(", slotType=").append(Objects.toString(slotType));
        str.append(", parents=");
        if (parents != null) {
          str.append(Arrays.toString(parents.toArray()));
        } else {
            str.append("null");
        }
        str.append(", children=");
        if (children != null) {
          str.append(Arrays.toString(children.toArray()));
        } else {
            str.append("null");
        }
        str.append(", powers=");
        if (powers != null) {
          str.append(Arrays.toString(powers.toArray()));
        } else {
            str.append("null");
        }
        str.append(", poweredBy=");
        if (poweredBy != null) {
          str.append(Arrays.toString(poweredBy.toArray()));
        } else {
            str.append("null");
        }
        str.append(", controls=");
        if (controls != null) {
          str.append(Arrays.toString(controls.toArray()));
        } else {
            str.append("null");
        }
        str.append(", controlledBy=");
        if (controlledBy != null) {
          str.append(Arrays.toString(controlledBy.toArray()));
        } else {
            str.append("null");
        }
        str.append("]");
        return str.toString();
    }

}


