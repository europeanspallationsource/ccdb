/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.jaxb;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This is data transfer object representing a CCDB slot for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@XmlRootElement(name = "slot")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ PropertyValueXml.class, ArtifactXml.class, SlotReferenceXml.class})
public class SlotXml extends SlotNameXml {
	private String description;

    @XmlElementWrapper(name = "artifacts")
    @XmlAnyElement(lax = true)
    private List<ArtifactXml> artifacts;

    @XmlElement 
    private String deviceType;

    @XmlElementWrapper(name = "properties")
    @XmlAnyElement(lax = true)
    private List<PropertyValueXml> properties;

    public SlotXml() { }
    
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public String getDeviceType() { return deviceType; }
    public void setDeviceType(String deviceType) { this.deviceType = deviceType; }

    public List<PropertyValueXml> getProperties() { return properties; }
    public void setProperties(List<PropertyValueXml> properties) { this.properties = properties; }

    public List<ArtifactXml> getArtifacts() { return artifacts; }
    public void setArtifacts(List<ArtifactXml> artifacts) { this.artifacts = artifacts; }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("SlotXml[");
        str.append("id=").append(id);
        str.append(", nameId=").append(nameId);
        str.append(", name=").append(name);
        str.append(", slotType=").append(Objects.toString(slotType));
        str.append(", description=").append(description);
        str.append(", deviceType=").append(deviceType);
        str.append(", artifacts=");
        if (artifacts != null) {
          str.append(Arrays.toString(artifacts.toArray()));
        } else {
            str.append("null");
        }
        str.append(", parents=");
        if (parents != null) {
          str.append(Arrays.toString(parents.toArray()));
        } else {
            str.append("null");
        }
        str.append(", children=");
        if (children != null) {
          str.append(Arrays.toString(children.toArray()));
        } else {
            str.append("null");
        }
        str.append(", powers=");
        if (powers != null) {
          str.append(Arrays.toString(powers.toArray()));
        } else {
            str.append("null");
        }
        str.append(", poweredBy=");
        if (poweredBy != null) {
          str.append(Arrays.toString(poweredBy.toArray()));
        } else {
            str.append("null");
        }
        str.append(", controls=");
        if (controls != null) {
          str.append(Arrays.toString(controls.toArray()));
        } else {
            str.append("null");
        }
        str.append(", controlledBy=");
        if (controlledBy != null) {
          str.append(Arrays.toString(controlledBy.toArray()));
        } else {
            str.append("null");
        }
        str.append(", properties=");
        if (properties != null) {
          str.append(Arrays.toString(properties.toArray()));
        } else {
            str.append("null");
        }
        str.append("]");
        return str.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((artifacts == null) ? 0 : artifacts.hashCode());
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((controlledBy == null) ? 0 : controlledBy.hashCode());
        result = prime * result + ((controls == null) ? 0 : controls.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((deviceType == null) ? 0 : deviceType.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((nameId == null) ? 0 : nameId.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parents == null) ? 0 : parents.hashCode());
        result = prime * result + ((poweredBy == null) ? 0 : poweredBy.hashCode());
        result = prime * result + ((powers == null) ? 0 : powers.hashCode());
        result = prime * result + ((properties == null) ? 0 : properties.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SlotXml other = (SlotXml) obj;
        if (artifacts == null) {
            if (other.artifacts != null)
                return false;
        } else if (!artifacts.equals(other.artifacts))
            return false;
        if (children == null) {
            if (other.children != null)
                return false;
        } else if (!children.equals(other.children))
            return false;
        if (controlledBy == null) {
            if (other.controlledBy != null)
                return false;
        } else if (!controlledBy.equals(other.controlledBy))
            return false;
        if (controls == null) {
            if (other.controls != null)
                return false;
        } else if (!controls.equals(other.controls))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (deviceType == null) {
            if (other.deviceType != null)
                return false;
        } else if (!deviceType.equals(other.deviceType))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (nameId == null) {
            if (other.nameId != null)
                return false;
        } else if (!nameId.equals(other.nameId))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (parents == null) {
            if (other.parents != null)
                return false;
        } else if (!parents.equals(other.parents))
            return false;
        if (poweredBy == null) {
            if (other.poweredBy != null)
                return false;
        } else if (!poweredBy.equals(other.poweredBy))
            return false;
        if (powers == null) {
            if (other.powers != null)
                return false;
        } else if (!powers.equals(other.powers))
            return false;
        if (properties == null) {
            if (other.properties != null)
                return false;
        } else if (!properties.equals(other.properties))
            return false;
        return true;
    }
}
