/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.jaxrs;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.openepics.discs.conf.jaxb.DeviceTypeXml;
import org.openepics.discs.conf.jaxb.SlotXml;
import org.openepics.discs.conf.jaxb.lists.SlotListXml;

/**
 * This resource provides bulk and specific installation slot data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("slots")
public interface SlotResource {
    /**
     * This method returns all the {@link SlotXml}s in the database or their subset based on
     * the {@link DeviceTypeXml}.
     *
     * @param deviceType optional {@link DeviceTypeXml} name
     * @return {@link SlotListXml}
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getInstallationSlots(
            @DefaultValue("undefined") @QueryParam("deviceType") String deviceType);

    /**
     * Returns a specific installation slot based on the id provided.
     *
     * @param id
     *            the id of the installation slot to retrieve
     * @return the installation slot instance data
     */
    @GET
    @Path("id/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getInstallationSlotById(@PathParam("id") Long id);

    /**
     * Returns a specific installation slot based on the name id provided.
     *
     * @param nameId
     *            the name id of the installation slot to retrieve
     * @return the installation slot instance data
     */
    @GET
    @Path("nameId/{nameId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getInstallationSlotByNameId(@PathParam("nameId") String nameId);

    /**
     * Returns the subset of {@link SlotXml}s based on the name provided.
     *
     * @param name
     *            the name of the installation slot to retrieve
     * @return {@link SlotListXml}
     */
    @GET
    @Path("{name}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getInstallationSlotsByName(@PathParam("name") String name);

    /**
     * Returns a specific installation slot artifact file based on the id provided.
     *
     * @param id
     *            the id of the installation slot from which to retrieve
     *            artifact file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the installation slot artifact file
     */
    @GET
    @Path("id/{id}/download/{fileName}")
    @Produces({ MediaType.MEDIA_TYPE_WILDCARD })
    public Response getAttachmentById(@PathParam("id") Long id, @PathParam("fileName") String fileName);

    /**
     * Returns a specific installation slot artifact file based on the name id provided.
     *
     * @param nameId
     *            the name id of the installation slot from which to retrieve
     *            artifact file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the installation slot artifact file
     */
    @GET
    @Path("nameId/{nameId}/download/{fileName}")
    @Produces({ MediaType.MEDIA_TYPE_WILDCARD })
    public Response getAttachmentByNameId(@PathParam("nameId") String nameId, @PathParam("fileName") String fileName);

    /**
     * Returns a specific installation slot artifact file based on the name provided.
     *
     * @param name
     *            the name of the installation slot from which to retrieve
     *            artifact file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the installation slot artifact file
     */
    @GET
    @Path("{name}/download/{fileName}")
    @Produces({ MediaType.MEDIA_TYPE_WILDCARD })
    public Response getAttachmentByName(@PathParam("name") String name, @PathParam("fileName") String fileName);

    /**
     * @param id the id of the installation slot
     * @param transitive
     *              whether to walk the relationship hierarchy (<code>true</code>)
     *              or only check immediate children (<code>false</code> - default).
     * @param properties the {@link List} of properties the slot must contain
     * @return all the slots that match the parameters
     */
    @GET
    @Path("id/{id}/controls")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getControlsChildrenById(@PathParam("id") Long id,
            @DefaultValue("false") @QueryParam("transitive") boolean transitive,
            @QueryParam("properties") List<String> properties);

    /**
     * @param nameId the name id of the installation slot
     * @param transitive
     *              whether to walk the relationship hierarchy (<code>true</code>)
     *              or only check immediate children (<code>false</code> - default).
     * @param properties the {@link List} of properties the slot must contain
     * @return all the slots that match the parameters
     */
    @GET
    @Path("nameId/{nameId}/controls")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getControlsChildrenByNameId(@PathParam("nameId") String nameId,
            @DefaultValue("false") @QueryParam("transitive") boolean transitive,
            @QueryParam("properties") List<String> properties);

    /**
     * @param name the name of the installation slot
     * @param transitive
     *              whether to walk the relationship hierarchy (<code>true</code>)
     *              or only check immediate children (<code>false</code> - default).
     * @param properties the {@link List} of properties the slot must contain
     * @return all the slots that match the parameters
     */
    @GET
    @Path("{name}/controls")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getControlsChildrenByName(@PathParam("name") String name,
            @DefaultValue("false") @QueryParam("transitive") boolean transitive,
            @QueryParam("properties") List<String> properties);
}
