package org.openepics.discs.conf.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a referenceb data to the installation slots and slot names
 * for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name = "name")
@XmlAccessorType(XmlAccessType.FIELD)
public class SlotReferenceXml {
    private Long id;
    private String nameId;
    private String name;

    public SlotReferenceXml() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNameId() { return nameId; }
    public void setNameId(String nameId) { this.nameId = nameId; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("SlotReferenceXml[");
        str.append("id=").append(id);
        str.append(", nameId=").append(nameId);
        str.append(", name=").append(name);
        str.append("]");
        return str.toString();
    }
}
