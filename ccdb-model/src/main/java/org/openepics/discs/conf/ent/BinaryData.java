/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.ent;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author georgweiss
 * Created Oct 10, 2018
 */
@Entity
@Table(name = "binary_data")
public class BinaryData implements Serializable{
    
    private static final long serialVersionUID = 5644646133210627110L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    protected Long id;
    
    @Basic(optional = false, fetch = FetchType.LAZY)
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "content")
    private byte[] content;
    
    @Basic(optional = false)
    @Column(name = "content_length")
    private int contentLength;
    
    public BinaryData(){
        
    }
    
    public BinaryData(byte[] content){
        this.content = content;
        if(content != null){
            this.contentLength = content.length;
        }
    }
    
    public Long getId(){
        return id;
    }
    
    public int getContentLength(){
        return contentLength;
    }
    
    public void setContent(byte[] content){
        this.content = content;
        if(content != null){
            contentLength = content.length;
        }
    }
    
    public byte[] getContent(){
        return content;
    }
}
