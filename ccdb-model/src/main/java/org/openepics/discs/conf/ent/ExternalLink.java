/** 
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.ent;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A class encapsulating an external link, i.e. basically an URL.
 *
 * @author Georg Weiss, ESS
 *
 */
@Embeddable
public class ExternalLink implements Serializable{

    private static final long serialVersionUID = 944434688368463730L;
     
    /**
     * The id column is not for JPA. It is needed by business logic
     * to enforce uniqueness for the name property.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "id")
    private String id;
    
    @Basic(optional = false)
    @Column(name = "modified_at")
    //@Temporal(TemporalType.TIMESTAMP)
    protected Date modifiedAt = new Date(0L);

    @Basic(optional = false)
    @Size(min = 1, max = 64)
    @Column(name = "modified_by")
    protected String modifiedBy;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "description")
    private String description;
    
    @Basic(optional = true)
    @Column(name = "uri", columnDefinition = "TEXT")
    private String uri;

    public ExternalLink() {
        
    } 

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
     public String getUri(){
        return uri;
    }
    
    public void setUri(String uri){
        this.uri = uri;
    }
    
    public void setId(String id){
        this.id = id;
    }
    
    public String getId(){
        return id;
    }
    
      /** @return The timestamp of the last modification of this database entity */
    public Date getModifiedAt() {
        return new Date(modifiedAt.getTime());
    }
    /**
     * The setter stores a new copy of the param.
     *
     * @param modifiedAt The timestamp of the last modification of this database entity
     */
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = new Date(modifiedAt.getTime());
    }

    /** @return The user performing the last modification of the database entity */
    public String getModifiedBy() {
        return modifiedBy;
    }
    /** @param modifiedBy The user performing the last modification of the database entity */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object other){
        if(other instanceof ExternalLink){
            return ((ExternalLink)other).getName().equals(name);
        }
        return false;
    }
    
    @Override
    public int hashCode(){
        return name.hashCode();
    }
    
    public static ExternalLink createNewCopy(ExternalLink source){
        ExternalLink target = new ExternalLink();
        target.setId(UUID.randomUUID().toString());
        target.setName(source.getName());
        target.setDescription(source.getDescription());
        target.setModifiedAt(source.getModifiedAt());
        target.setModifiedBy(source.getModifiedBy());
        target.setUri(source.getUri());
        
        return target;
    }
}
