package org.openepics.discs.client;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.openepics.discs.conf.jaxb.DeviceTypeXml;
import org.openepics.discs.conf.jaxb.DeviceXml;
import org.openepics.discs.conf.jaxb.SlotXml;
import org.openepics.discs.conf.jaxrs.client.DeviceClient;
import org.openepics.discs.conf.jaxrs.client.DeviceTypeClient;
import org.openepics.discs.conf.jaxrs.client.InstallationSlotClient;

import com.google.common.collect.Lists;

@Ignore
public class ClientTest {

    private void dumpList(final String name, final List<?> list) {
        if (list != null) {
            System.out.print(name + ": ");
            System.out.println(Arrays.toString(list.toArray()));
        }
    }

    @Test
    public void testSlots() {
        final InstallationSlotClient client = CCDBClient.createInstallationSlotClient();

        final List<SlotXml> slots =  client.getInstallationSlots();
        dumpSlots(slots);
    }

    private void dumpSlots(final List<SlotXml> slots) {
        if (slots == null) {
            System.out.println("slots == null");
            return;
        }
        System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
        System.out.println("slots: " + slots.size());
        for (final SlotXml slot : slots) {
            System.out.println();
            System.out.println("id: " + slot.getId());
            System.out.println("nameId: " + slot.getNameId());
            System.out.println("name: " + slot.getName());
            System.out.println("description: " + slot.getDescription());
            System.out.println("deviceType: " + slot.getDeviceType());
            dumpList("artifacts", slot.getArtifacts());
            dumpList("parents", slot.getParents());
            dumpList("children", slot.getChildren());
            dumpList("powers", slot.getPowers());
            dumpList("poweredBy", slot.getPoweredBy());
            dumpList("controls", slot.getControls());
            dumpList("controlledBy", slot.getControlledBy());
            dumpList("properties", slot.getProperties());
        }
    }

    @Test
    public void testTypes() {
        final DeviceTypeClient client =CCDBClient.createDeviceTypeClient();

        final List<DeviceTypeXml> types =  client.getAllDeviceTypes();
        dumpTypes(types);
    }

    private void dumpTypes(final List<DeviceTypeXml> types) {
        if (types == null) {
            System.out.println("types == null");
            return;
        }
        System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
        System.out.println("types: " + types.size());
        dumpList("Types", types);
    }

    @Test
    public void testDevices() {
        final DeviceClient client = CCDBClient.createDeviceClient();

        final List<DeviceXml> devs =  client.getAllDevices();
        dumpDevices(devs);
    }

    @Test
    public void testControlsSlotsByName() {
        final InstallationSlotClient client = CCDBClient.createInstallationSlotClient();

        final List<SlotXml> slots = client.getControlsChildrenByName("LEBT-00:Ctrl-IOC-1X", true,
                                                                    Lists.newArrayList("EPICSSnippet","EPICSModule"));
        dumpSlots(slots);
    }

    private void dumpDevices(final List<DeviceXml> devices) {
        if (devices == null) {
            System.out.println("devices == null");
            return;
        }
        System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
        System.out.println("devices: " + devices.size());
        dumpList("Devices", devices);
    }
}
