/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.client.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MultivaluedHashMap;

import org.openepics.discs.client.CCDBClient;
import org.openepics.discs.conf.jaxb.SlotXml;
import org.openepics.discs.conf.jaxb.SlotNameXml;
import org.openepics.discs.conf.jaxb.lists.SlotListXml;
import org.openepics.discs.conf.jaxb.lists.SlotNameListXml;
import org.openepics.discs.conf.jaxrs.client.InstallationSlotClient;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * This is CCDB service client installation slot parser that is used to get data from server.
 * <p>All class methods are static.</p>
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>\
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */

public class InstallationSlotClientImpl implements InstallationSlotClient {
    private static final Logger LOG = Logger.getLogger(InstallationSlotClientImpl.class.getCanonicalName());

    private static final String COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT = "Couldn't retrieve data from service at ";

    private static final String PATH_SLOTS_BY_NAME = "slots";
    private static final String PATH_SLOTS_BY_NAME_ID = "slots/nameId";
    private static final String PATH_SLOT_NAMES = "slotNames";

    private static final String QUERY_PARAM_DEVICE_TYPE = "deviceType";
    private static final String QUERY_PARAM_TRANSITIVE = "transitive";
    private static final String QUERY_PARAM_PROPERTIES = "properties";

    @Nonnull private final CCDBClient client;

    public InstallationSlotClientImpl(CCDBClient client) { this.client = client; }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getInstallationSlots()
     */
    @Override
    public List<SlotXml> getInstallationSlots() {
        return getInstallationSlots(null);
    }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getInstallationSlots(java.lang.String)
     */
    @Override
    public List<SlotXml> getInstallationSlots(String deviceType) {
        LOG.fine("Invoking getInstallationSlots. deviceType=" + deviceType);

        final String url = client.buildUrl(PATH_SLOTS_BY_NAME);

        final MultivaluedHashMap<String, Object> queryParams = new MultivaluedHashMap<>();
        if (deviceType != null) {
            queryParams.put(QUERY_PARAM_DEVICE_TYPE, Arrays.asList(deviceType));
        }
        try (final ClosableResponse response = client.getResponse(url, queryParams)) {
            return nonNullList(response.readEntity(SlotListXml.class).getInstallationSlots());
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getInstallationSlotByName(java.lang.String)
     */
    @Override
    public SlotXml getInstallationSlotByName(String name) {
        LOG.fine("Invoking getInstallationSlot. name=" + name);
        Preconditions.checkNotNull(name);

        final String url = client.buildUrl(PATH_SLOTS_BY_NAME, name);
        try (final ClosableResponse response = client.getResponse(url)) {
            List<SlotXml> installationSlots =
                    nonNullList(response.readEntity(SlotListXml.class).getInstallationSlots());
            if (installationSlots.isEmpty()) {
                return null;
            } else if (installationSlots.size() > 1) {
                LOG.severe("Multiple installation slots received, where only a single " +
                        "slot should exist.");
            }
            // In case multiple installation slots are returned, choose only the first one. In the future, we are
            // planning to modify this function to return all the results.
            return installationSlots.get(0);
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }
    
    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getInstallationSlotByNameId(java.lang.String)
     */
    @Override
    public SlotXml getInstallationSlotByNameId(String nameId) {
        LOG.fine("Invoking getInstallationSlot. nameId=" + nameId);
        Preconditions.checkNotNull(nameId);

        final String url = client.buildUrl(PATH_SLOTS_BY_NAME_ID, nameId);
        try (final ClosableResponse response = client.getResponse(url)) {
            SlotXml installationSlots = response.readEntity(SlotXml.class);
            return installationSlots;
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getAttachment(java.lang.String, java.lang.String)
     */
    @Override
    public InputStream getAttachment(String name, String fileName) {
        LOG.fine("Invoking getAttachment. name=" + name + ", fileName=" + fileName);
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(fileName);

        final String url = client.buildUrl(PATH_SLOTS_BY_NAME, name, "download", fileName);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(InputStream.class);
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    @Override
    public List<SlotNameXml> getAllInstallationSlotNames() {
        return getAllInstallationSlotNames(null);
    }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getAllInstallationSlotNames(java.lang.String)
     */
    @Override
    public List<SlotNameXml> getAllInstallationSlotNames(String deviceTypeName) {
        LOG.fine("Invoking getAllInstallationSlotNames. deviceTypeName=" + deviceTypeName);

        final String url = client.buildUrl(PATH_SLOT_NAMES);

        final MultivaluedHashMap<String, Object> queryParams = new MultivaluedHashMap<>();
        if (deviceTypeName != null) {
            queryParams.put(QUERY_PARAM_DEVICE_TYPE, Arrays.asList(deviceTypeName));
        }

        try (final ClosableResponse response = client.getResponse(url, queryParams)) {
            return nonNullList(response.readEntity(SlotNameListXml.class).getNames());
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getControlsChildrenByName(java.lang.String, boolean, java.util.List)
     */
    @Override
    public List<SlotXml> getControlsChildrenByName(String name, boolean transitive, List<String> properties) {
        LOG.fine("Invoking getControlsChildren. name=" + name + ", transitive=" + transitive + ", "
                + "properties=" + properties);
        Preconditions.checkNotNull(name);
        final String url = client.buildUrl(PATH_SLOTS_BY_NAME, name, "controls");
        return getControlsChildrenByUrl(url, transitive, properties);
    }
    
    private List<SlotXml> getControlsChildrenByUrl(String url, boolean transitive, List<String> properties) {
        final MultivaluedHashMap<String, Object> queryParams = new MultivaluedHashMap<>();
        queryParams.put(QUERY_PARAM_TRANSITIVE, Arrays.asList(Boolean.toString(transitive)));
        if ((properties != null) && !properties.isEmpty()) {
            queryParams.put(QUERY_PARAM_PROPERTIES, new ArrayList<Object>(properties));
        }

        try (final ClosableResponse response = client.getResponse(url, queryParams)) {
            return nonNullList(response.readEntity(SlotListXml.class).getInstallationSlots());
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }        
    }

    /* (non-Javadoc)
     * @see org.openepics.discs.conf.jaxrs.client.InstallationSlotClient#getControlsChildrenByNameId(java.lang.String, boolean, java.util.List)
     */
    @Override
    public List<SlotXml> getControlsChildrenByNameId(String nameId, boolean transitive, List<String> properties) {
        LOG.fine("Invoking getControlsChildren. nameId=" + nameId + ", transitive=" + transitive + ", "
                + "properties=" + properties);
        Preconditions.checkNotNull(nameId);

        final String url = client.buildUrl(PATH_SLOTS_BY_NAME_ID, nameId, "controls");
        return getControlsChildrenByUrl(url, transitive, properties);
    }

    private <T> List<T> nonNullList(List<T> list) {
        return (list != null) ? list : Lists.newArrayList();
    }
}
