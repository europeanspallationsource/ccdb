package org.openepics.discs.client.impl;

import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import org.openepics.discs.client.CCDBClient;
import org.openepics.discs.conf.jaxb.DeviceXml;
import org.openepics.discs.conf.jaxb.lists.DeviceListXml;
import org.openepics.discs.conf.jaxrs.client.DeviceClient;

import com.google.common.base.Preconditions;

public class DeviceClientImpl implements DeviceClient {

    private static final Logger LOG = Logger.getLogger(DeviceClientImpl.class.getCanonicalName());

    private static final String COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT = "Couldn't retrieve data from service at ";

    private static final String PATH_DEVICES = "devices";

    @Nonnull private final CCDBClient client;

    public DeviceClientImpl(CCDBClient client) {
        this.client = client;
    }

    /**
     * Requests a {@link DeviceListXml} containing a {@link List} of all {@link DeviceXml}s from the REST service.
     *
     * @throws ResponseException if data couldn't be retrieved
     *
     * @return {@link DeviceListXml}
     */
    @Override
    public List<DeviceXml> getAllDevices() {
        LOG.fine("Invoking getAllDevices");

        final String url = client.buildUrl(PATH_DEVICES);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(DeviceListXml.class).getDeviceList();
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    /**
     * Requests particular {@link DeviceXml} from the REST service.
     *
     * @param inventoryId the inventoryId of the desired {@link DeviceXml}
     *
     * @throws ResponseException if data couldn't be retrieved
     *
     * @return {@link DeviceXml}
     */
    @Override
    public DeviceXml getDevice(String inventoryId) {
        LOG.fine("Invoking getDevice. inventoryId=" + inventoryId);
        Preconditions.checkNotNull(inventoryId);

        final String url = client.buildUrl(PATH_DEVICES, inventoryId);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(DeviceXml.class);
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    @Override
    public InputStream getAttachment(String name, String fileName) {
        LOG.fine("Invoking getAttachment. name=" + name + ", fileName=" + fileName);
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(fileName);

        final String url = client.buildUrl(PATH_DEVICES, name, "download", fileName);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(InputStream.class);
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }
}
