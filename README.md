# Controls Configuration Database

The Controls Configuration Database (CCDB) enables the collection, storage, and distribution of
(static) controls configuration data needed to install, commission and operate the European
Spallation Source (ESS) control system.

More specifically, the CCDB manages the information of thousands of (physical and logical) devices
such as cameras, PLCs, IOCs, racks, crates, etc…, that will be in operation at ESS by defining
their properties and relationships between these from the control system perspective.

This information is then consumed both by end-users and other ICS applications
(e.g. Cable Database, IOC Factory, Csentry) to enable these to successfully perform their domain
specific businesses.


## Project organization

The source in the repository code is split into multiple sub-projects. The Maven groupId
for the project is "org.openepics.discs":
- ccdb-model: the JPA object model (artifactId:ccdb-model)
- ccdb-business: the application business logic; EJBs and supporting classes (artifactId:ccdb-business)
- ccdb-JAXB: the JAXB classes used with RESTful service and the description of the service interface (artifactId:ccdb-jaxb)
- ccdb-client-api: the client API other EE applications can use to communicate with the CCDB REST web service (artifactId:ccdb-client-api)
- conf-core: the user Java EE web application (artifactId:confmgr)
- ccdb-ws: web service implmentation (artifactId:ccdb-ws)

Each module is versioned independently, except the conf-core and ccdb-ws version numbers
are synced. The ccdb-model, ccdb-business, ccdb-JAXB and ccdb-client-api are all deployed
into the Maven repository. All sub-projects are tagged independently. The tag naming
rules are as follows:
- ccdb-model: <version>-model (1.0.2-model)
- ccdb-business: <version>-business (1.3.8-business)
- ccdb-JAXB: <version>-JAXB (1.0.0-JAXB)
- ccdb-client-api: <version>-client-api (1.0.0-client-api)
- ccdb-ws, conf-core: <version> (1.0.4)

## Docker

The application can be built and run as a Docker container. The image is based on the jboss/wildfly image from Docker Hub.

### How to use this image

```
$ docker run registry.esss.lu.se/ics-software/ccdb
```

Environment variables that can be set when running a container based on this image:

| Environment variable     | Default    | Description |
| -------------------------|------------|-------------|
| CCDB_DATABASE_HOST | ccdb-postgres | Host used for the database connection |
| CCDB_DATABASE_PORT | 5432 | Port used for the database connection |
| CCDB_DATABASE_NAME | discs_ccdb | Database name used for the database connection |
| CCDB_DATABASE_USERNAME | discs_ccdb | Username used for the database connection |
| CCDB_DATABASE_PASSWORD | discs_ccdb | Password used for the database connection |
| RBAC_PRIMARY_URL | https://rbac.esss.lu.se:8443/service | URL for primary RBAC service |
| RBAC_PRIMARY_SSL_HOST | rbac.esss.lu.se | SSL host for primary RBAC service |
| RBAC_PRIMARY_SSL_PORT | 8443 | SSL port for primary RBAC service |
| RBAC_SINGLE_SIGNON | false | Use single sign-on |
| RBAC_SECONDARY_URL | https://localhost:8443/service | URL for secondary RBAC service |
| RBAC_SECONDARY_SSL_HOST | localhost | SSL host for secondary RBAC service |
| RBAC_SECONDARY_SSL_PORT | 8443 | SSL port for secondary RBAC service |
| RBAC_HANDSHAKE | true | Perform SSL handshake with RBAC |
| RBAC_HANDSHAKE_TIMEOUT | 2000 | Timeout for SSL handshake with RBAC |
| RBAC_INACTIVITY_TIMEOUT_DEFAULT | 900 | Inactivity timeout for RBAC client |
| RBAC_INACTIVITY_RESPONSE_GRACE | 30 | Inactivity response grace period for RBAC client |
| RBAC_SHOW_ROLE_SELECTOR | false | Show role selector in RBAC client |
| RBAC_VERIFY_SIGNATURE | false | Verify signature of RBAC |
| RBAC_PUBLIC_KEY_LOCATION | ~/.rbac/rbac.key | Public key for RBAC |
| RBAC_LOCAL_SERVICES_PORT | 9421 | Port for local RBAC service |
| RBAC_USE_LOCAL_SERVICE | false | Use local RBAC service |
| RBAC_CERTIFICATE_STORE | ~/.rbac | SSL certificate store for RBAC client |
| RBAC_COOKIE_DOMAIN | .esss.lu.se | RBAC Cookie domain |
| RBAC_LDAP_SECURITY_CREDENTIALS |  | LDAP bind password |
| CCDB_DEPLOYMENT_CONTEXT_ROOT_CONF | / | Context root used for the web application |
| CCDB_DEPLOYMENT_CONTEXT_ROOT_WS | /rest | Context root used for the REST application |
| NAMING_APP_URL | https://naming.esss.lu.se | URL for Naming web application |
| NAMING_SERVICES_BASE_URL | https://naming.esss.lu.se/rest | URL for Naming REST services |
| CABLEDB_URL | https://cable.esss.lu.se | URL for Cable-db application |
| RBAC_LDAP_SECURITY_CREDENTIALS |  | LDAP bind password |
| CABLE_APPLICATION_URL | https://cable.esss.lu.se/ | URL for Cable-db web application |
| CABLE_SERVICE_URL | https://cable.esss.lu.se/rest | URL for Cable-db REST services |
| CHESS_URL | https://chess.esss.lu.se/enovia/link/essName/ | URL for Chess |
| CCDB_DATABASE_MIGRATION | true | Run Flyway migration upon application startup |

### Docker Compose

For convenience, the application comes with a docker-compose.yml file, which can be used to run the application with required services and configuration:

```
$ docker-compose up
```
Note: To be able to run the application, the environment variable RBAC_LDAP_SECURITY_CREDENTIALS must be added with the correct password for the LDAP bind.
