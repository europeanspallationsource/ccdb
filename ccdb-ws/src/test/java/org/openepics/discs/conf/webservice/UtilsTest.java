/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.webservice;

import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.Artifact;
import org.openepics.discs.conf.jaxb.ArtifactXml;

import static org.junit.Assert.*;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.openepics.discs.conf.ent.EntityWithExternalLinks;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.jaxb.PropertyKindXml;

/**
 *
 * @author georgweiss
 * Created Sep 13, 2018
 */
@SuppressWarnings("javadoc")
public class UtilsTest {

    @Test
    public void testGetArtifactListFromEntity(){
        
        ComptypeArtifact ga1 = new ComptypeArtifact();
        ga1.setName("artifact1");
        ga1.setDescription("desc1");
        ComptypeArtifact ga2 = new ComptypeArtifact();
        ga2.setName("artifact2");
        ga2.setDescription("desc");
       
        List<ComptypeArtifact> artifactList = Arrays.asList(ga1, ga2);
        
        EntityWithArtifacts entity = new EntityWithArtifacts() {
            @Override
            public List<ComptypeArtifact> getEntityArtifactList() {
                return artifactList;
            }
        };
        
        List<ArtifactXml> artifacts = Utils.getArtifacts(entity);

        assertEquals(2, artifacts.size());
        assertEquals("artifact1", artifacts.get(0).getName());
        assertEquals("desc1", artifacts.get(0).getDescription());
        assertEquals(PropertyKindXml.DEVICE, artifacts.get(0).getKind());
    }
    
    @Test
    public void testGetExternalLinkListFromEntity(){
        
        ExternalLink ea1 = new ExternalLink();
        ea1.setName("artifact1");
        ea1.setDescription("desc1");
        ea1.setUri("uri1");
        ExternalLink ea2 = new ExternalLink();
        ea2.setName("artifact2");
        ea2.setDescription("desc2");
        ea2.setUri("uri2");
       
        Set<ExternalLink> artifactList = Sets.newHashSet(ea1, ea2);
        
        EntityWithExternalLinks entity = () -> {return artifactList;};
        
        List<ArtifactXml> artifacts = Utils.getExternalLinks(entity);
        
        assertEquals(2, artifacts.size());
        assertEquals("artifact1", artifacts.get(0).getName());
        assertEquals(PropertyKindXml.DEVICE, artifacts.get(0).getKind());
        assertEquals("uri1", artifacts.get(0).getUri());
    }
}
