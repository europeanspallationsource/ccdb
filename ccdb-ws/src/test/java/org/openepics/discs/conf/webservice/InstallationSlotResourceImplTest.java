/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.webservice;

import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.openepics.discs.conf.ent.ExternalLink;
import org.openepics.discs.conf.ent.GenericArtifact;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotArtifact;
import org.openepics.discs.conf.jaxb.ArtifactXml;

/**
 *
 * @author georgweiss
 * Created Sep 21, 2018
 */
public class InstallationSlotResourceImplTest {
    
    private SlotResourceImpl installationSlotResourceImpl = new SlotResourceImpl();
    
    private ComptypeArtifact ga1;
    private ComptypeArtifact ga2;
    private ComptypeArtifact ga3;
    
    private ExternalLink el1;
    private ExternalLink el2;
    private ExternalLink el3;
    
    @Before
    public void setUp(){
        ga1 = new ComptypeArtifact();
        ga1.setName("ga1");
        ga2 = new ComptypeArtifact();
        ga2.setName("ga2");
        ga3 = new ComptypeArtifact();
        ga3.setName("ga3");
        
        el1 = new ExternalLink();
        el1.setName("el1");
        el2 = new ExternalLink();
        el2.setName("el2");
        el3 = new ExternalLink();
        el3.setName("el3");
    }
    
    
    @Test
    public void testGetArtifactsNullRecordOneExternalLink(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1));
        
        slot.setComponentType(componentType);
        
        List<ArtifactXml> artifacts = installationSlotResourceImpl.getArtifacts(slot, null);
        
        assertEquals(1, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsNullRecordTwoExternalLinks(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1, el2));
        
        slot.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = installationSlotResourceImpl.getArtifacts(slot, null);
        
        assertEquals(2, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsNullRecordExternalLinksAndArtifacts(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1, el2));
        componentType.setEntityArtifactList(Arrays.asList(ga1, ga2));
        
        slot.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = installationSlotResourceImpl.getArtifacts(slot, null);
        
        assertEquals(4, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsNullRecordNoArtifacts(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
       
        slot.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = installationSlotResourceImpl.getArtifacts(slot, null);
        
        assertEquals(0, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsElementsInAllNodes(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1));
        componentType.setEntityArtifactList(Arrays.asList(ga1));
        
        InstallationRecord installationRecord = 
                new InstallationRecord("installationRecord", new Date());
        
        Slot slot2 = new Slot("slot2", true);
        
        slot2.setExternalLinkList(Sets.newHashSet(el2));
        SlotArtifact sa1 = new SlotArtifact("sa1", "desc");
        slot2.setEntityArtifactList(Arrays.asList(sa1));
        
        installationRecord.setSlot(slot2);
        
        slot.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = installationSlotResourceImpl.getArtifacts(slot, installationRecord);
        
        assertEquals(4, artifacts.size());
    }
    
    @Test
    public void testGetArtifactsElementsInAllNodes2(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
        componentType.setExternalLinkList(Sets.newHashSet(el1));
        componentType.setEntityArtifactList(Arrays.asList(ga1));
        
        SlotArtifact sa = new SlotArtifact("sa", "desc");
        slot.setEntityArtifactList(Arrays.asList(sa));
       
        slot.setExternalLinkList(Sets.newHashSet(el3));
        
        InstallationRecord installationRecord = 
                new InstallationRecord("installationRecord", new Date());
        
        Slot slot2 = new Slot("slot2", true);
        
        slot2.setExternalLinkList(Sets.newHashSet(el2));
        SlotArtifact sa1 = new SlotArtifact("sa1", "desc");
        slot2.setEntityArtifactList(Arrays.asList(sa1));
        
        
        installationRecord.setSlot(slot2);
        slot.setComponentType(componentType);      
        
        List<ArtifactXml> artifacts = installationSlotResourceImpl.getArtifacts(slot, installationRecord);
        
        assertEquals(6, artifacts.size());
    }
}
