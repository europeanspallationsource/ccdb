/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.webservice;

import com.google.common.collect.Sets;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Date;
import javax.ws.rs.core.Response;
import org.junit.Test;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.GenericArtifact;

import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifierType;
import static org.openepics.discs.conf.webservice.GetAttachmentResourceBase.AttachmentOwnerIdentifier;
import static org.junit.Assert.*;
import org.openepics.discs.conf.ent.BinaryData;
import org.openepics.discs.conf.ent.ComptypeArtifact;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.DeviceArtifact;
import org.openepics.discs.conf.ent.InstallationArtifact;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotArtifact;

/**
 *
 * @author georgweiss
 * Created Sep 13, 2018
 */
@SuppressWarnings("javadoc")
public class GetAttachmentResourceBaseTest {

    @Test
    public void testGetFileForDeviceType(){
        
        ComponentType componentType = new ComponentType();
        byte[] content = new byte[]{1,2,3};
        
        ComptypeArtifact artifact = new ComptypeArtifact();
        artifact.setName("artifact1");
        artifact.setDescription("desc");
        artifact.setBinaryData(new BinaryData(content));
        componentType.setEntityArtifactList(Arrays.asList(artifact));

        AttachmentOwnerIdentifier ownerIdentifier = 
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, "foo");
        Response response = 
                GetAttachmentResourceBase.getFileForDeviceType(componentType, ownerIdentifier, "artifact1");
        
        byte[] fileContents = new byte[3];
        ((ByteArrayInputStream)response.getEntity()).read(fileContents, 0, 3);
        
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertArrayEquals(fileContents, content);
        
    }
    
    @Test
    public void testGetFileForDeviceTypeAttachmentNotFound(){
        ComponentType componentType = new ComponentType();
        byte[] content = new byte[]{1,2,3};
        
        GenericArtifact artifact = new GenericArtifact();
        artifact.setName("artifact1");
        artifact.setDescription("desc");
        artifact.setContent(content);
        componentType.setArtifactList(Sets.newHashSet(artifact));

        AttachmentOwnerIdentifier ownerIdentifier = 
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, "foo");
        Response response = 
                GetAttachmentResourceBase.getFileForDeviceType(componentType, ownerIdentifier, "artifact2");
        
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testGetFileForSlotNoContenmtInAnyEntity(){
        
         Slot slot = new Slot("slot", true);
         ComponentType componentType = new ComponentType();
         slot.setComponentType(componentType);

        AttachmentOwnerIdentifier ownerIdentifier = 
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, "slot");
         Response response =
                 GetAttachmentResourceBase.getFileForSlot(slot, null, ownerIdentifier, "file");
         
         assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());     
    }
    
    @Test
    public void testGetFileForSlotContentInSlot(){
        
         Slot slot = new Slot("slot", true);
         
          
         ComponentType componentType = new ComponentType();
         slot.setComponentType(componentType);
         
         byte[] content = new byte[]{1,2,3};
        
        SlotArtifact artifact = new SlotArtifact();
        artifact.setName("artifact1");
        artifact.setDescription("desc");
        artifact.setBinaryData(new BinaryData(content));
        slot.setEntityArtifactList(Arrays.asList(artifact));

        AttachmentOwnerIdentifier ownerIdentifier = 
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, "slot");
        Response response = GetAttachmentResourceBase.getFileForSlot(slot, null, ownerIdentifier, 
                "artifact1");
         
         assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());  
         
         byte[] fileContents = new byte[3];
        ((ByteArrayInputStream)response.getEntity()).read(fileContents, 0, 3);
        
        assertArrayEquals(fileContents, content);

    }
    
    @Test
    public void testGetFileForSlotContentInDeviceOfInstllationRecord(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
        slot.setComponentType(componentType);
         
        byte[] content = new byte[]{1,2,3};
        
        DeviceArtifact artifact = new DeviceArtifact("artifact1", "desc");
        
        artifact.setBinaryData(new BinaryData(content));
       
        InstallationRecord installationRecord =
                new InstallationRecord("record number", new Date());
        Device device = new Device("123");
        device.setEntityArtifactList(Arrays.asList(artifact));
        
        installationRecord.setDevice(device);

        AttachmentOwnerIdentifier ownerIdentifier = 
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, "slot");
        Response response =
                 GetAttachmentResourceBase.getFileForSlot(slot, installationRecord, ownerIdentifier, "artifact1");
         
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());  
         
        byte[] fileContents = new byte[3];
        ((ByteArrayInputStream)response.getEntity()).read(fileContents, 0, 3);
        
        assertArrayEquals(fileContents, content);

    }
    
     @Test
    public void testGetFileForSlotContentInComponentTypeOfSlot(){
        
        Slot slot = new Slot("slot", true);
        ComponentType componentType = new ComponentType();
         
        byte[] content = new byte[]{1,2,3};
        
        ComptypeArtifact artifact = new ComptypeArtifact();
        artifact.setName("artifact1");
        artifact.setDescription("desc");
        artifact.setBinaryData(new BinaryData(content));
        componentType.setEntityArtifactList(Arrays.asList(artifact));
        
        slot.setComponentType(componentType);

        AttachmentOwnerIdentifier ownerIdentifier = 
                new AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType.NAME, "slot");
        Response response =
                 GetAttachmentResourceBase.getFileForSlot(slot, null, ownerIdentifier, "artifact1");
         
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());  
         
        byte[] fileContents = new byte[3];
        ((ByteArrayInputStream)response.getEntity()).read(fileContents, 0, 3);
        
        assertArrayEquals(fileContents, content);

    }
}
