package org.openepics.discs.conf.helpers;

import javax.ws.rs.core.Response;

public class ResponseHelper {
    public static <T> Response createResponse(T entity) {
        if (entity == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.OK).entity(entity).build();
    } 
}
