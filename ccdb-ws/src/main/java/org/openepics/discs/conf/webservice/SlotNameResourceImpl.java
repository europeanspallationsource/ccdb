/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;


import java.io.Serializable;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.openepics.discs.conf.ejb.ComptypeEJB;
import org.openepics.discs.conf.ejb.SlotEJB;
import org.openepics.discs.conf.ejb.SlotPairEJB;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.jaxb.SlotNameXml;
import org.openepics.discs.conf.jaxb.SlotTypeXml;
import org.openepics.discs.conf.jaxb.lists.SlotNameListXml;
import org.openepics.discs.conf.jaxrs.SlotNameResource;
import org.openepics.discs.conf.util.Utility;

import com.google.common.base.Strings;

/**
 * An implementation of the InstallationSlotBasicResource interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class SlotNameResourceImpl implements SlotNameResource, Serializable {
    private static final long serialVersionUID = -1014268826571453590L;

    @Inject private SlotEJB slotEJB;
    @Inject private SlotPairEJB slotPairEJB;
    @Inject private ComptypeEJB comptypeEJB;

    private SlotHieararchyCache slotHieararchyCache;
    
    @Override
    public Response getAllInstallationSlotNames(String deviceTypeName) {
    	try
    	{
	        slotHieararchyCache = new SlotHieararchyCache(slotPairEJB);
            SlotNameListXml installationSlotNames = new SlotNameListXml(
	                Strings.isNullOrEmpty(deviceTypeName)
	                            ? slotEJB.findAll().stream().
	                                    filter(s -> !s.getComponentType().getName().equals(SlotEJB.ROOT_COMPONENT_TYPE)).
	                                    map(slot -> createSlotName(slot)).collect(Collectors.toList())
	                            : Utility.nullableToStream(comptypeEJB.findByName(deviceTypeName)).
	                                    flatMap(compType -> slotEJB.findByComponentType(compType).stream()).
	                                    filter(s -> !s.getComponentType().getName().equals(SlotEJB.ROOT_COMPONENT_TYPE)).
	                                    map(slot -> createSlotName(slot)).collect(Collectors.toList())
	                );
            return Response.status(Response.Status.OK).entity(installationSlotNames).build();
    	} finally {
	        slotHieararchyCache = null;
    	}
    }
    
    private SlotNameXml createSlotName(final Slot slot) {
        if (slot == null) {
            return null;
        }

        final SlotNameXml installationSlotName = new SlotNameXml();
        installationSlotName.setId(slot.getId());
        installationSlotName.setNameId(slot.getNameUuid());
        installationSlotName.setName(slot.getName());
        installationSlotName.setSlotType(slot.isHostingSlot() ? SlotTypeXml.SLOT : SlotTypeXml.CONTAINER);

        final Long id = slot.getId();
        installationSlotName.setParents(Utils.getSlotNames(slotHieararchyCache.getParents(id)));
        installationSlotName.setChildren(Utils.getSlotNames(slotHieararchyCache.getChildren(id)));
        installationSlotName.setPoweredBy(Utils.getSlotNames(slotHieararchyCache.getPoweredBy(id)));
        installationSlotName.setPowers(Utils.getSlotNames(slotHieararchyCache.getPowers(id)));
        installationSlotName.setControlledBy(Utils.getSlotNames(slotHieararchyCache.getControlledBy(id)));
        installationSlotName.setControls(Utils.getSlotNames(slotHieararchyCache.getControls(id)));
        
        return installationSlotName;
    }
}
