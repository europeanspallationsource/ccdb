package org.openepics.discs.conf.db;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;

@Singleton
@Startup
@TransactionManagement(value = TransactionManagementType.BEAN)
public class DatabaseMigration {

    private static final Logger LOGGER = Logger.getLogger(DatabaseMigration.class.getName());

    @Resource(lookup = "java:/org.openepics.discs.conf.data")
    private DataSource dataSource;

    @PostConstruct
    private void onStartup() {
        if ("true".equalsIgnoreCase(System.getProperty("flyway.enabled"))) {
            if (dataSource == null) {
                throw new EJBException("DataSource could not be found!");
            }

            Map<String, String> placeholders = new HashMap<>();
            System.getProperties().entrySet().forEach(e -> {
                String key = (String) e.getKey();
                String value = (String) e.getValue();
                if (key.startsWith("flyway.placeholders.")) {
                    placeholders.put(key.substring(20), value);
                }
            });

            Flyway flyway = new Flyway();
            flyway.setPlaceholders(placeholders);
            flyway.setDataSource(dataSource);

            MigrationInfo migrationInfo = flyway.info().current();
            if (migrationInfo == null) {
                LOGGER.log(Level.INFO, "Database is not versioned");
            } else {
                LOGGER.log(Level.INFO, "Database contains data version: " + migrationInfo.getVersion() + " : " + migrationInfo.getDescription());
            }

            flyway.migrate();
            LOGGER.log(Level.INFO, "Database migrated to data version: " + flyway.info().current().getVersion());
        }
    }
}
